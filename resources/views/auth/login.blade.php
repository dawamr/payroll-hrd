@extends('layouts.auth')
@section('content')
<?php
$config = \App\Config::first();
?>
<div class="container">
        <div class="col-md-12 content-center">
            <div class="card-plain">
                <form class="form" action="{{route('login')}}" method="POST">
                    @csrf
                    <div class="header">
                        <div class="logo-container">
                            @if(!empty($config) )
                            <img src="/uploads/{{$config['logo_aplikasi']}}" alt="">
                            @else
                            Logo App
                            @endif
                        </div>
                        <h5>Log in</h5>
                    </div>
                    <div class="content">                                                
                        <div class="input-group input-lg">
                            <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email Address">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-account-circle"></i>
                            </span>
                        </div>
                        
                        <div class="input-group input-lg">
                            <input type="password" id="password" name="password" placeholder="Password" class="form-control" />
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-lock"></i>
                            </span>
                        </div>
                    </div>
                    <div class="footer text-center">
                        <button class="btn btn-primary btn-round btn-lg btn-block " id="loginNow">SIGN IN</button>
                        <h5><a href="{{ route('password.request') }}" class="link">Forgot Password?</a></h5>
                        <div class="text-right">
                            <ul>
                                @error('email') <li>Email or Password Incorect</li> @enderror
                                @error('password') <li>Password Incorect</li> @enderror
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
@endsection
