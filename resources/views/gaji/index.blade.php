@extends('layouts.master')

@section('style')
<!-- JQuery DataTable Css -->
<link rel="stylesheet" href="/oreo/assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css">
<link href="/oreo/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<!-- Select2 -->
<link rel="stylesheet" href="/oreo/assets/plugins/select2/select2.css" />

<style>
.btn-primary-hover{
    transition: background 1s;
}
.btn-primary-hover:hover{
    background:#6572b8;
    color:white;
}
.wizard > .content{
    background :#fff;
}
.box{
    -webkit-overflow-scrolling: touch;
    overflow-x: scroll;
    overflow-y: hidden;
    white-space: nowrap;
}
.box-1{
    display:inline-block;
}
</style>

@endsection

@section('content')
<div class="container">
    <div class="row clearfix" id="AppTablePosition">
        <div class="col-sm-12">
            <div class="card">                       
                <div class="header">
                    <h2><strong>List</strong> Posisi </h2>
                    <ul class="header-dropdown">
                        <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                            <ul class="dropdown-menu slideUp float-right">
                                <li><a href="javascript:void(0);"></a></li>
                                <li><a href="javascript:void(0);"></a></li>
                                <li><a href="javascript:void(0);"></a></li>
                            </ul>
                        </li>
                        <li class="remove">
                            <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <?php  
                        $bmul = \Carbon\Carbon::createFromFormat('d/m/Y',Date('d/m/Y'))->format('m');
                        $nmBulan = array(1=>'Januari',2=>'Febuari',3=>'Maret',4=>'April',5=>'Mei',6=>'Juni',7=>'Juli',8=>'Agustus',9=>'September',10=>'Oktober',11=>'November',12=>'Desember');
                        for($i=1;$i<13;$i++)
                            if($bmul == $i){$bmul = $nmBulan[$i];} ?> 
                    <b>Periode : {{$bmul}} {{Date('Y')}}</b> 
                    </div>
                    <div class="container">
                        <div class="table-responsive" id="tables">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>NIP</th>
                                        <th>Gaji Kotor</th>
                                        <th>Potongan Kehadiran</th>
                                        <th>Total Pendapatan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Nama</th>
                                        <th>NIP</th>
                                        <th>Gaji Kotor</th>
                                        <th>Potongan Kehadiran</th>
                                        <th>Total Pendapatan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach($pegawai as $row)
                                    <tr>
                                        <?php
                                            $awalbln = new \Carbon\Carbon('first day of this month');
                                            $start = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$awalbln)->format('Y-m-d');
                                            $akhirbln = new \Carbon\Carbon('last day of this month');
                                            $end = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$akhirbln)->format('Y-m-d');
                                            
                                            $presence = \App\Presence::where('user_id',$row->id)->whereBetween('tgl', [$start,$end])->get();

                                            $jab = \App\Position::find($row->jabatan_id); 
                                            $jml_absen = count($presence);
                                            $total1 = (($jab->gj_pokok + $jab->gj_fungsional + $jab->gj_kinerja)-($jab->bpjs_kesehatan + $jab->bpjs_pensiun + $jab->bpjs_tenkerja));
                                            $potongan = (($total1*24)/730)*$jml_absen;
                                            $total = ($total1 - $potongan);
                                        ?>
                                        <td>{{$row->nama}}</td>
                                        <td>{{$row->nip}}</td>
                                        <td>Rp. {{number_format($jab->gj_pokok)}},-</td>
                                        <td>Rp. {{number_format($potongan)}},-</td>
                                        <td>Rp. {{number_format($total)}},-</td>
                                        <td class="text-center" ><a href="#tables" onclick="window.open('/salary/{{$row->id}}')"><i class="fas fa-external-link-alt"></i></a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
@endsection
@section('script')
<!-- Large Size -->
<!-- Jquery DataTable Plugin Js --> 
<script src="/oreo/light/assets/bundles/datatablescripts.bundle.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.print.min.js"></script>

$(document).ready(function(){
    
});

</script>
<!-- Bootstrap Material Datetime Picker Plugin Js --> 
<!-- <script src="/oreo/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>  -->

<script src="/oreo/light/assets/js/pages/tables/jquery-datatable.js"></script>
@endsection