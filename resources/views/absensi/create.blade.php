@extends('layouts.master')
@section('title')
<title>APP_Name - Employees</title>
@endsection

@section('style')
<link href="/oreo/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<link href="/oreo/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<!-- Select2 -->
<link rel="stylesheet" href="/oreo/assets/plugins/select2/select2.css" />

<style>
.btn-primary-hover{
    transition: background 1s;
}
.btn-primary-hover:hover{
    background:#6572b8;
    color:white;
}
.wizard > .content{
    background :#fff;
}
.box{
    -webkit-overflow-scrolling: touch;
    overflow-x: scroll;
    overflow-y: hidden;
    white-space: nowrap;
}
.box-1{
    display:inline-block;
}
</style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix" id="AppTablePresence">
            <div class="col-sm-12">
                <div class="card">                       
                    <div class="header">
                        <h2><strong>Ijin</strong> Pegawai </h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu slideUp float-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else</a></li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form class="form">
                            {{csrf_field()}}
                            <div class="body"> 
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group col-md-12 form-float"><span>NIP</span>
                                            <input type="text" class="form-control select2" placeholder="NIP *" name="nip"  id="inputNip" required>
                                        </div>
                                        <div class="form-group col-md-12 form-float"><span>Keterangan</span>
                                            <select class="form-control" name="keterangan" id="inputKet">
                                                <option value="s">Sakit</option>
                                                <option value="i">Ijin</option>
                                                <option value="a">Alpha</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-12 form-float"><span>Dari Tanggal</span>
                                            <input type="text" class="form-control datetimepicker" placeholder="Tanggal" id="inputTgl" required>
                                        </div>
                                        <div class="form-group col-md-12 form-float"><span>Lama Hari</span>
                                            <input type="number" class="form-control" name="lama" placeholder="Lama Ketidakhadiran" id="inputLama">
                                        </div>
                                        <div class="col-md-12form-group pull-right pl-2 pr-2">
                                            <button class="btn btn-simple btn-round waves-effect" id="btCancel">Batal</button>
                                            <button class="btn btn-info btn-round waves-effect" type="submit" id="simpanData"> Simpan </button>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group col-md-12 form-float"><span>Nama</span>
                                            <input type="text" class="form-control" placeholder="Nama *" name="nama"  id="inputNama" required>
                                        </div>
                                        <div class="form-group col-md-12 form-float"><span>Jabatan</span>
                                            <input type="text" class="form-control" placeholder="Jabatan *" name="jabatan"  id="inputJab" required>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <img id="profile" width="150px" height="200px" src="" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class=" ml-3">
                                <div class="form-group pull-right pl-2 pr-2">
                                    <button class="btn btn-simple btn-round waves-effect" id="btCancel">Batal</button>
                                    <button class="btn btn-info btn-round waves-effect" type="submit" id="simpanData"> Simpan </button>
                                </div>
                                {{-- <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button> --}}
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
        
        <!-- #END# Advanced Form Example With Validation --> 
    </div>
@endsection
@section('script')

<script src="/oreo/assets/plugins/momentjs/moment.js"></script> 
<!-- Jquery DataTable Plugin Js --> 
<script src="/oreo/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script> 
<script src="/oreo/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> <!-- Input Mask Plugin Js --> 
<script src="/oreo/assets/plugins/select2/select2.min.js"></script> <!-- Select2 Js -->
<script>
    $(document).ready(function(){
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
        ////////////////////////////////
        
        $('.datetimepicker').bootstrapMaterialDatePicker({
            format: 'DD/MM/Y',
            clearButton: true,
            weekStart: 1,
            time: false
        });
        var dd;
        $('#inputNip').keyup((e)=>{
            e.preventDefault();
            $.ajax({
                url :'/api/byNip/'+$('#inputNip').val()+'' ,
                method : "GET",
                dataType : 'json',
                encode : true
            }).done((data)=>{
                if(data.length >0){
                    console.log(data)
                    $('#inputNama').val(data[0].nama);
                    $('#inputJab').val(data[0].jabatan);
                    $('#profile').attr('src', '/uploads/'+data[0].foto)
                    $('#inputTgl').val(<?php Date('dmY') ?>);
                    dd = data[0].id;
                }else{
                    $('#inputNama').val('');
                    $('#inputJab').val('');
                    $('#inputTgl').val('');
                    $('#inputLama').val('');
                }
            })  
        });
        $('#btCancel').click(()=>{
            location.href = '/presence'
        })
        $('#simpanData').click((e)=>{
            e.preventDefault();
            if($('#inputNama').val().length =0){return 0}
            if($('#inputLama').val().length =0){return 0}
            if($('#inputJab').val().length =0){return 0}
            if($('#inputTgl').val().length =0){return 0}
            if($('#inputKet').val().length =0){return 0}
            $.ajax({
                url :'/presence' ,
                method : "POST",
                data: {
                    _token : $('input[name="_token"]').val(),
                    nama:$('#inputNama').val(),
                    id:dd,
                    jabatan: $('#inputJab').val(),
                    tgl: $('#inputTgl').val(),
                    ket: $('#inputKet').val(),
                    lama: $('#inputLama').val()
                }
            }).done((data)=>{
                location.href = '/presence';
            })
        });
            
        
        // $('#inputNip').select2((data)=>{
        //     allowClear: true,
        //     multiple: true,
        //     maximumSelectionSize: 1,
        //     placeholder: "Start typing",
        //     data: data    
        // });
    });

</script>
<!-- Bootstrap Material Datetime Picker Plugin Js --> 
<!-- <script src="/oreo/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>  -->

<script src="/oreo/light/assets/js/pages/tables/jquery-datatable.js"></script>
@endsection