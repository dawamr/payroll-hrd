@extends('layouts.master')
@section('title')
<title>APP_Name - Employees</title>
@endsection

@section('style')
<style>
.btn-primary-hover{
    transition: background 1s;
}
.btn-primary-hover:hover{
    background:#6572b8;
    color:white;
}
.wizard > .content{
    background :#fff;
}
.box{
    -webkit-overflow-scrolling: touch;
    overflow-x: scroll;
    overflow-y: hidden;
    white-space: nowrap;
}
.box-1{
    display:inline-block;
}
</style>
<link href="/oreo/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link rel="stylesheet" href="/datatables/dataTables.bootstrap4.css">
<link rel="stylesheet" href="/datatables/responsive.bootstrap4.css">
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix" id="AppTableDetail">
            <div class="col-sm-12">
                <div class="card">                       
                    <div class="header">
                        <h2 style="font:arial"><strong>Detail</strong> Kehadiran </h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu slideUp float-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else</a></li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="card">
                            <form action="/presence/s/{{$id}}" method="POST">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="">Dari Tanggal</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="zmdi zmdi-calendar"></i>
                                            </span>
                                            <input type="text" class="form-control datetimepicker" name="tgl_str" id="tgl_str" value="{{$mulai}}" placeholder="Dari Tanggal">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label for="">Sampai Tanggal</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="zmdi zmdi-calendar"></i>
                                            </span>
                                            <input type="text" class="form-control datetimepicker" name="tgl_end" id="tgl_end" value="{{$sampai}}" placeholder="Sampai Tanggal">
                                        </div>
                                    </div>
                                    <div class="col col-md-3">
                                        <button class="btn btn-round btn-block btn-primary" type="submit">Tampilkan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="col-12 text-center">
                        <?php
                        $bmul = \Carbon\Carbon::createFromFormat('d/m/Y',$mulai)->format('m');
                        $bsel = \Carbon\Carbon::createFromFormat('d/m/Y',$sampai)->format('m');
                        $nmBulan = array(1=>'Januari',2=>'Febuari',3=>'Maret',4=>'April',5=>'Mei',6=>'Juni',7=>'Juli',8=>'Agustus',9=>'September',10=>'Oktober',11=>'November',12=>'Desember');
                        for($i=1;$i<13;$i++)
                            if($bmul == $i){$bmul = $nmBulan[$i];}
                        for($i=1;$i<13;$i++)
                            if($bsel == $i){$bsel = $nmBulan[$i];}
                        ?>
                        
                        <h4>Bulan {{$bmul}} - Bulan {{$bsel}}</h4>
                            <div class="col-12">
                                <b>{{$user->nama}}</b><br>
                                NIP. {{$user->nip}}
                            </div>
                            <br>
                            <table class="table">
                                <tr>
                                    <td>Sakit</td>
                                    <td>Ijin</td>
                                    <td>Alpha</td>
                                    <td>Total</td>
                                </tr>
                                <tr>
                                    <td>{{count($sakit)}}</td>
                                    <td>{{count($ijin)}}</td>
                                    <td>{{count($alpha)}}</td>
                                    <td>{{(count($sakit)+count($ijin)+count($alpha))}}</td>
                                </tr>
                            </table>
                        <hr>
                    </div>
                </div>

                <div class="card">
                    <div class="col-sm-12 table-responsive"> 
                        <h4 class="text-center">Rincian </h4>
                        <table class="table table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Keterangan</th>
                                    <th>Tanggal</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Keterangan</th>
                                    <th>Tanggal</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <?php $no = 1 ?>
                                @foreach($presence as $row)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>
                                        @if($row->keterangan == 's') Sakit @endif
                                        @if($row->keterangan == 'i') Ijin @endif
                                        @if($row->keterangan == 'a') Alpha @endif
                                    </td>
                                    <td>{{\Carbon\Carbon::createFromFormat('Y-m-d',$row->tgl)->format('d/m/Y')}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> 
        
        <!-- #END# Advanced Form Example With Validation --> 
    </div>
@endsection
@section('script')
<script src="/oreo/assets/plugins/momentjs/moment.js"></script> 
<script src="/oreo/light/assets/bundles/datatablescripts.bundle.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.print.min.js"></script>
<script src="/oreo/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script> 
<script>
    $(document).ready(function(){
        let _token = $('input[name="_token"]').val();

        $('.dataTable').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
        $('.datetimepicker').bootstrapMaterialDatePicker({
            format: 'DD/MM/Y',
            clearButton: true,
            weekStart: 1,
            time: false
        });
    });
</script>
@endsection