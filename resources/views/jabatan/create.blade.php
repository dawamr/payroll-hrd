@extends('layouts.master')
@section('title')
<title>APP_Name - Employees</title>
@endsection

@section('style')
<style>
.btn-primary-hover{
    transition: background 1s;
}
.btn-primary-hover:hover{
    background:#6572b8;
    color:white;
}
.wizard > .content{
    background :#fff;
}
.box{
    -webkit-overflow-scrolling: touch;
    overflow-x: scroll;
    overflow-y: hidden;
    white-space: nowrap;
}
.box-1{
    display:inline-block;
}
</style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix" id="AppCreatePosition">
            <div class="col-sm-12">
                <div class="card">                       
                    <div class="header">
                        <h2><strong>Edit</strong> Posisi <span class="nama_posisi"></span> </h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu slideUp float-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else</a></li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body" style="margin-left:1em;margin-right:1em">
                        
                        <form>
                            {{csrf_field()}}
                            <div class="row">
                                <div class="form-group col-md-4 form-float"><span>Kode Jabatan</span>
                                    <input type="text" class="form-control" placeholder="Kode Jabatan *" name="kode"  id="edKode" required>
                                    <input type="hidden" name="id" id="frid">
                                </div>
                                <div class="form-group col-md-8 form-float"><span>Jabatan</span>
                                    <input type="text" class="form-control" placeholder="Jabatan *" name="jabatan"  id="edJabatan" required>
                                    <input type="hidden" name="id" id="frid">
                                </div>
                                <div class="form-group col-md-4 form-float"><span>Gaji Pokok</span>
                                    <input type="text" class="form-control money gaji" placeholder="Rp. *" name="gjPokok"  id="edGapok" required>
                                </div>
                                <div class="form-group col-md-4 form-float"><span>Tunjangan Fungsional</span>
                                    <input type="text" class="form-control money gaji" placeholder="Rp. *" name="gjFungsional"  id="edGafung" required>
                                </div>
                                <div class="form-group col-md-4 form-float"><span>Tunjangan Kinerja</span>
                                    <input type="text" class="form-control money " placeholder="Rp. *" name="gjKinerja" value="0" id="edGakin" required>
                                </div>
                                <hr>
                            </div>
                        </form>
                    </div>
                    <div class="footer">
                        <div class="form-group" style="margin-left:1.5em">
                            <a href="#" id="btBatalTambah" class="btn btn-secondary btn-round">Batal</a>
                            <button class="btn btn-info btn-round" id="btSimpanTambah" type="submit"> Simpan </button>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        
        <!-- #END# Advanced Form Example With Validation --> 
    </div>
@endsection
@section('script')
<script src="/js/simple.money.format.js"></script>
<script>
$(document).ready(function(){
    let _token = $('input[name="_token"]').val();
    $('.money').simpleMoneyFormat();
    $('#btBatalTambah').click(()=>{
        $('#AppCreatePosition');
        location.replace('/position').fadeIn();
    })
    $('.gaji').eq(0).val(0);$('.gaji').eq(1).val(0);
    $('.gaji').eq(0).keyup(function(){
        var nilai = (parseInt($(this).val().split(',').join('')) + parseInt($('.gaji').eq(1).val().split(',').join('')))/100;
        $('.bpjs').eq(0).val(nilai).simpleMoneyFormat();
        $('.bpjs').eq(1).val(nilai).simpleMoneyFormat();
        $('.bpjs').eq(2).val(nilai).simpleMoneyFormat();
    });
    $('.gaji').eq(1).keyup(function(){
        var nilai = (parseInt($(this).val().split(',').join('')) + parseInt($('.gaji').eq(0).val().split(',').join('')))/100;
        $('.bpjs').eq(0).val(nilai).simpleMoneyFormat();
        $('.bpjs').eq(1).val(nilai).simpleMoneyFormat();
        $('.bpjs').eq(2).val(nilai).simpleMoneyFormat()
        // console.log(nilai)
    });
    $('#btSimpanTambah').click((e)=>{
        e.preventDefault();
        $.ajax({
            url :"/position",
            method : "POST",
            data :{
                _token:_token,
                kode: $('#edKode').val(),
                jabatan: $('#edJabatan').val(),
                gaj_pokok:$('#edGapok').val(),
                gj_fung:$('#edGafung').val(),
                gaj_tjKinerja:$('#edGakin').val(),
                bpjs : $('#edBkes').val()
                
            }, success:function(result){
                location.href = '/position'
            }                      
        });
    })
})
</script>
@endsection