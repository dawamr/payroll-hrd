@extends('layouts.master')
@section('title')
<title>APP_Name - Employees</title>
@endsection
<?php

$myreg = \App\Region::get();

?>
@section('style')

<!-- JQuery DataTable Css -->
<link rel="stylesheet" href="{{asset('oreo/assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}"/>
<link rel="stylesheet" href="{{asset('oreo/assets/plugins/jquery-steps/jquery.steps.css')}}"/>
<link rel="stylesheet" href="{{asset('oreo/assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>

<!-- Select2 -->
<link rel="stylesheet" href="{{asset('oreo/assets/plugins/select2/select2.css')}}"/>

<style>
.btn-primary-hover{
    transition: background 1s;
}
.btn-primary-hover:hover{
    background:#6572b8;
    color:white;
}
.wizard > .content{
    background :#fff;
}
.box{
    -webkit-overflow-scrolling: touch;
    overflow-x: scroll;
    overflow-y: hidden;
    white-space: nowrap;
}
.box-1{
    display:inline-block;
}
</style>
@endsection
 
@section('content')
    <div class="container-fluid">
        <div class="row clearfix" id="AppTableEmployee">
            <div class="col-sm-12">
                <div class="card">                       
                    <div class="header">
                        <h2><strong>List</strong> Employees </h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu slideUp float-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else</a></li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">    
                        <div class="col-3 btn-primary-hover" id="btAdd" >
                            <button class="btn btn-primary btn-icon btn-round hidden-sm-down m-l-10" type="button">
                                <i class="zmdi zmdi-plus"></i>
                            </button>  Tambah Pegawai  
                        </div>
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>NIP</th>
                                    <th>Jabatan</th>
                                    <th>Tempat Lahir</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Pendidikan</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Nama</th>
                                    <th>NIP</th>
                                    <th>Jabatan</th>
                                    <th>Tempat Lahir</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Pendidikan</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach($pegawai as $data)
                                <tr>
                                    <?php $name = \App\Position::find($data->jabatan_id) ?>
                                    <td><a href="#" url="/workers/{{$data->id}}"  class="tdNama">{{$data->nama}}</a></td>
                                    <td>{{$data->nip}}</td>
                                    <td>{{$name['nama']}}</td>
                                    <td>{{ $data->tmp_lahir}}</td>
                                    <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d',$data->tgl_lahir)->format('l, d/m/Y')}}</td>
                                    <td>{{strtoupper($data->pendidikan)}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> 
        <!-- Advanced Form Example With Validation -->
        <div class="row clearfix"  id="AppAddEditEmployee">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Input</strong> New Employee</h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else</a></li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form enctype="multipart/form-data">
                            <div class="body">                                     
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <form>
                                        {{csrf_field()}}
                                        <div role="tabpanel" class="tab-pane in active" id="step1"> <h6>Input Data Pegawai</h6>
                                            
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <div class="form-group form-float"><span>NIP</span>
                                                        <input type="text" class="form-control" placeholder="NIP *" name="nip"  id="fr1" required>
                                                    </div>
                                                    <div class="form-group form-float"><span>Nama Lengkap</span>
                                                        <input type="text" class="form-control" placeholder="Nama Lengkap *" name="nama"  id="fr2" required>
                                                    </div>
                                                    <div class="form-group form-float"><span>Tempat Lahir</span>
                                                        <input type="text" class="form-control" placeholder="Tempat Lahir *" name="tmpLahir" id="fr3" required>
                                                    </div>
                                                    <div class="form-group form-float"><span>Tanggal Lahir</span>
                                                        <input type="text" class="form-control date" placeholder="Ex: 30/07/2016" id="fr4" required>
                                                    </div>
                                                    <div class="form-group form-float" > <div id="selectJK">
                                                    <span>Jenis Kelamin</span>
                                                        <select class="form-control"a id="fr5" required>
                                                            <option value="">-- Pilih Jenis Kelamin * --</option>
                                                            <option value="L">Laki - Laki</option>
                                                            <option value="P">Perempuan</option>
                                                        </select></div>
                                                    </div>
                                                    
                                                    <div class="form-group form-float" id="selectAgamax">
                                                    
                                                    <div id="regid"></div>                                                        
                                                    </div>
                                                    <div class="form-group form-float"><span>Alamat</span>
                                                        <textarea style="border: 1px solid #E3E3E3; border-radius: 30px;" name="" class="form-control form-float" placeholder="Alamat" id="fr7" required></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 img-post">
                                                    <center>
                                                        <b>Foto</b><br>
                                                        <img class="target img-responsive mt-3" src="" alt=""></center>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="step2"> <h6>Input Data Pegawai</h6>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-9">
                                                <div class="form-group form-float">
                                                    <div id="jabatanid"></div>
                                                    </div>
                                                    <div class="form-group form-float"><span>Tanggal Bergabung</span>
                                                        <input type="text" class="form-control date" name="joinDate" placeholder="Join Date (30/07/2016)" id="fr9" required>
                                                    </div>
                                                    <div class="form-group form-float">
                                                        <div id="pendidikanid"></div>
                                                    </div>
                                                    <div class="form-group form-float"><span>Nomor Rekening</span>
                                                        <input type="text" class="form-control" placeholder="Nomor Rekening *" name="rek" id="fr11" required>
                                                    </div>
                                                    <div class="form-group form-float"><span>Nama Bank</span>
                                                        <input type="text" class="form-control"  placeholder="Nama Bank *" name="bank" id="fr12" required>
                                                    </div>
                                                    <div class="form-group form-float">
                                                        <div id="statusid"></div>
                                                    </div>
                                                    <div class="form-group form-float"><span>Unggah Foto</span>
                                                        <input type="file" class="form-control" placeholder="Upload Photo *" name="foto" id="foto" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 img-post">
                                                    <center>
                                                        <b>Foto</b><br>
                                                        <img class="target img-responsive" src="" alt="">
                                                    </center>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item"><a class="btn btn-primary" data-toggle="tab" id="steps0"> </a> </li>
                                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" id="steps1" href="#step1"> 1 </a></li>
                                    <li class="nav-item"><a class="nav-link" data-toggle="tab" id="steps2" href="#step2"> 2 </a></li>
                                    <li class="nav-item"><button class="btn btn-primary" data-toggle="tab" type="submit" id="steps3"> Create New </button></li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix" id="AppShowEmployee">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Profile </strong>Nama </h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else</a></li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-md-4 text-center">
                                <div class="profile-image">
                                    <img src="{{asset('oreo/assets/images/profile_av.jpg')}}" alt="" style="border-radius:50%"  class="img-responsive mt-4"><hr>
                                </div> 
                                <h5>Nama</h5>
                                <b>Position</b>
                                <hr>
                                <div class="post-toolbar-b">
                                    <button id="btCancel" data-toggle="tooltip" data-placement="bottom" title="Kembali" class="btn btn-secondary btn-icon  btn-icon-mini btn-round"><i class="material-icons">chevron_left</i></button>
                                    <button id="btEdit" url="" data-toggle="tooltip" data-placement="bottom" title="Edit" class="btn btn-warning btn-icon  btn-icon-mini btn-round"><i class="material-icons">mode_edit</i></button>
                                    <form action="" id="frDelete" method="post">
                                    @method('DELETE')
                                    {{csrf_field()}}
                                        <button type="submit" data-toggle="tooltip" data-placement="bottom" title="Hapus" class="btn btn-danger btn-icon  btn-icon-mini btn-round"><i class="material-icons">delete_forever</i></button>
                                    </form>
                                </div>
                                <hr>
                            </div>
                            <div class="col-md-4">
                                <small class="text-muted">NIP: </small>
                                <p id="nip"></p>
                                <hr>
                                <small class="text-muted">Nama Lengkap: </small>
                                <p id="fullname"></p>
                                <hr>
                                <small class="text-muted">Tempat Lahir: </small>
                                <p id="tmp_lahir"></p>
                                <hr>
                                <small class="text-muted">Tanggal Lahir: </small>
                                <p id="tgl_lahir" class="m-b-0"></p>
                                <hr>
                                <small class="text-muted">Jenis Kelamin: </small>
                                <p id="jk"></p>
                                <hr>
                                <small class="text-muted">Agama: </small>
                                <p id="agama"></p>
                                <hr>
                                <small class="text-muted">Alamat: </small>
                                <p id="alamat"></p>
                                <hr>
                                <small class="text-muted">Jabatan: </small>
                                <p id="position" class="m-b-0"></p>
                            </div>
                            <div class="col-md-4">
                                <small class="text-muted">Tanggal Bergabung: </small>
                                <p id="tgl_bergabung"></p>
                                <hr>
                                <small class="text-muted">Pendidikan Terakhir: </small>
                                <p id="pendidikan"></p>
                                <hr>
                                <small class="text-muted">Nomor Rekening: </small>
                                <p id="no_rek"></p>
                                <hr>
                                <small class="text-muted">Bank: </small>
                                <p>Bank <span id="bank"></span></p>
                                <hr>
                                <small class="text-muted">Status Perkawinan: </small>
                                <p id="status"></p>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Advanced Form Example With Validation --> 
    </div>
@endsection
@section('script')

<!-- Jquery DataTable Plugin Js --> 
<script src="{{asset('oreo/light/assets/bundles/datatablescripts.bundle.js')}}"></script>
<script src="{{asset('oreo/assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('oreo/assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('oreo/assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
<script src="{{asset('oreo/assets/plugins/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
<script src="{{asset('oreo/assets/plugins/jquery-datatable/buttons/buttons.print.min.js')}}"></script>

<script src="{{asset('oreo/assets/plugins/jquery-validation/jquery.validate.js')}}"></script> <!-- Jquery Validation Plugin Css -->
<script src="{{asset('oreo/assets/plugins/jquery-steps/jquery.steps.js')}}"></script> <!-- JQuery Steps Plugin Js -->

<script src="{{asset('oreo/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js')}}"></script> <!-- Input Mask Plugin Js --> 
<script src="{{asset('oreo/assets/plugins/select2/select2.min.js')}}"></script> <!-- Select2 Js -->
<script>
    $(document).ready(function(){
        //////////////////////////////
        $('#AppTableEmployee').show();
        $('#AppTableEmployee').fadeIn();
        $('#AppAddEditEmployee').hide();
        $('#AppShowEmployee').hide();
        ///////////////////////////////
        $('.tdNama').click(function(e){
            e.preventDefault();
            let url = $(this).attr('url')
            $.ajax({
                url :url,
                method : "GET",
                dataType : 'json',
                encode : true
            }).done((data)=>{
                $('#btEdit').attr("url",url);
                $('#frDelete').attr("action",url);
                $('#nip').html(data.data.nip);
                $('#img_profile').attr('src','/uploads/'+data.data.foto);
                $('#fullname').html(data.data.nama);
                $('#tmp_lahir').html(data.data.tmp_lahir);
                $('#tgl_lahir').html(data.data.tgl_lahir);
                $('#agama').html(data.agama.agama.charAt(0).toUpperCase() + data.agama.agama.slice(1));
                $('#alamat').html(data.data.alamat);                
                $('#position').html(data.data.jabatan); 
                $('#tgl_bergabung').html(data.data.join);
                $('#pendidikan').html(data.pendidikan);
                $('#no_rek').html(data.data.rekening);
                $('#bank').html(data.data.bank);
                $('#status').html(data.data.status.charAt(0).toUpperCase() + data.data.status.slice(1));
                $('#fr1').val(data.data.nip);
                $('#fr2').val(data.data.nama);
                $('#fr3').val(data.data.tmp_lahir);
                $('#fr4').val(data.tgl_lahir);
                if(data.data.jk == 'L'){
                    $('#jk').html('Laki-laki');
                    $('#selectJK').replaceWith(
                        '<span>Jenis Kelamin</span><select class="form-control" id="fr5" required><option value="">-- Pilih Jenis Kelamin * --</option><option value="L" selected>Laki - Laki</option><option value="P">Perempuan</option></select>'
                    );
                }
                if(data.data.jk == 'P'){
                    $('#jk').html('Perempuan');
                    $('#selectJK').replaceWith(
                        '<span>Jenis Kelamin</span><select class="form-control" id="fr5" required><option value="">-- Pilih Jenis Kelamin * --</option><option value="L">Laki - Laki</option><option value="P" selected>Perempuan</option></select>'
                    );
                }
                var addreg = "";
                

                <?php foreach($myreg as $reg){ ?>
                    if(data.agama.agama == "<?=$reg->agama?>"){
                        
                        addreg += '<option value="<?=$reg->id?>" selected><?=$reg->agama?></option>';
                    }else{
                        addreg += '<option value="<?=$reg->id?>"><?=$reg->agama?></option>';
                    }
                <?php } ?>


                
                $('#regid').html('<span>Agama</span><select class="form-control" name="agama" id="fr6" required>'+addreg+'</select>');
                
                var addjab = "";

                <?php foreach($position as $jab){ ?>
                    if(data.data.jabatan_id == <?=$jab->id?>){
                        
                        addjab += '<option value="<?=$jab->id?>" selected><?=$jab->nama?></option>';
                    }else{
                        addjab += '<option value="<?=$jab->id?>"><?=$jab->nama?></option>';
                    }
                <?php } ?>


                
                $('#jabatanid').html('<span>Jabatan</span><select class="form-control" name="jabatan" id="fr8" required>'+addjab+'</select>');
                
                <?php
                    $myedu = array("sma" => "SMA / SMK", "s1" => "Sarjana (S1)", "s2" => "Sarjana (S2)", "s3" => "Sarjana (S3)");
                ?>

                var addedu = "";

                <?php foreach($myedu as $key => $data){ ?>
                
                    if(data.data.pendidikan == "<?=$key?>"){
                        
                        addedu += '<option value="<?=$key?>" selected><?=$data?></option>';
                    }else{
                        addedu += '<option value="<?=$key?>"><?=$data?></option>';
                    }
                <?php } ?>



                $('#pendidikanid').html('<span>Pendidikan</span><select class="form-control" name="pendidikan" id="fr10" required>'+addedu+'</select>');

                <?php
                    $mystat = array("menikah" => "Menikah", "lajang" => "Belum Menikah");
                ?>

                var addstat = "";

                <?php foreach($mystat as $key => $data){ ?>
                
                    if(data.data.pendidikan == "<?=$key?>"){
                        
                        addstat += '<option value="<?=$key?>" selected><?=$data?></option>';
                    }else{
                        addstat += '<option value="<?=$key?>"><?=$data?></option>';
                    }
                <?php } ?>



                $('#statusid').html('<span>Status</span><select class="form-control" name="status" id="fr13" required>'+addstat+'</select>');



                $('#fr7').val(data.data.alamat);
                
                $('#fr9').val(data.data.join);
                
                $('#fr11').val(data.data.rekening);
                $('#fr12').val(data.data.bank);
            });
            $('#AppTableEmployee').hide();
            $('#AppShowEmployee').fadeIn();
            $('#AppShowEmployee').show();
            $('#AppAddEditEmployee').hide();
        });
        $('#btCancel').click(function(){
            $('#AppTableEmployee').show();
            $('#AppTableEmployee').fadeIn();
            $('#AppAddEditEmployee').hide();
            $('#AppShowEmployee').hide();
        });
        $('#btEdit').click(function(){
            let url = $(this).attr('url')
            location.replace(url+'/edit'); 
        });
        $('#btAdd').click(function(){
            location.replace('/workers/create'); 
        });
        ///////////////////////////////
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
        //Date
        $('.date').inputmask('dd/mm/yyyy', { placeholder: '__/__/____' , inputFormat: "dd/mm/yyyy"});

        $('#jabatan').select2();
    });

</script>
<!-- Bootstrap Material Datetime Picker Plugin Js --> 
<!-- <script src="/oreo/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>  -->

<script src="{{asset('oreo/light/assets/js/pages/tables/jquery-datatable.js')}}"></script>
<script src="{{asset('oreo/light/assets/js/pages/forms/form-wizard.js')}}"></script>
@endsection