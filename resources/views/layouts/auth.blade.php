<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

    <title>@if(!empty($config)) {{$config['nama_aplikasi']}} - @endif {{Date('Y')}}</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- Custom Css -->
    <link rel="stylesheet" href="/oreo/assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/oreo/light/assets/css/main.css">
    <link rel="stylesheet" href="/oreo/light/assets/css/authentication.css">
    <link rel="stylesheet" href="/oreo/light/assets/css/color_skins.css">

    @yield('style')

</head>

<body class="theme-purple authentication sidebar-collapse">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top navbar-transparent">
    <div class="container">        
        <div class="navbar-translate n_logo">
            <a class="navbar-brand logoApp" href="javascript:void(0);" title="" target="_blank">@if(!empty($config)) {{$config['ama_aplikasi']}} @else APP Name @endif</a>
            <button class="navbar-toggler" type="button">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
            </button>
        </div>
    </div>
</nav>
<!-- End Navbar -->
<div class="page-header">
    <div class="page-header-image" style="background-image:url(/oreo/assets/images/login.jpg)"></div>
    @yield('content')
    <footer class="footer">
        <div class="container">
            
            <div class="copyright">
                &copy;
                <script>
                    document.write(new Date().getFullYear())
                </script>,
                <span>Created by <a href="/" target="_blank">@if(!empty($config)) {{$config['nama_aplikasi']}} @endif</a></span>
            </div>
        </div>
    </footer>
</div>

<!-- Jquery Core Js -->
<script src="/oreo/light/assets/bundles/libscripts.bundle.js"></script>
<script src="/oreo/light/assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

@yield('script')
<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
//=============================================================================
$('.form-control').on("focus", function() {
    $(this).parent('.input-group').addClass("input-group-focus");
}).on("blur", function() {
    $(this).parent(".input-group").removeClass("input-group-focus");
});
</script>
</body>
</html>