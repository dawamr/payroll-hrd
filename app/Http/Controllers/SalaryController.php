<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pegawai'] = \App\Employee::get();
        $data['jabatan'] = \App\Position::get();
        return view('gaji.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $awalbln = new \Carbon\Carbon('first day of this month');
        $start = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$awalbln)->format('Y-m-d');
        $akhirbln = new \Carbon\Carbon('last day of this month');
        $end = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$akhirbln)->format('Y-m-d');
        $user = \App\Employee::find($id);
        $presence = \App\Presence::where('user_id',$user->id)->whereBetween('tgl', [$start,$end])->get();

        $jab = \App\Position::find($user->jabatan_id); 
        $jml_absen = count($presence);
        $total1 = (($jab->gj_pokok + $jab->gj_fungsional + $jab->gj_kinerja)-($jab->bpjs_kesehatan + $jab->bpjs_pensiun + $jab->bpjs_tenkerja));
        $potongan = (($total1*24)/730)*$jml_absen;
        $total = ($total1 - $potongan);
        $data['total'] = $total;
        $data['jab'] = $jab;
        $data['start'] = $start;
        $data['end'] = $end;
        $data['potongan'] = $potongan;
        $data['user'] = $user;
        return view('pdf.slip-gaji')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
