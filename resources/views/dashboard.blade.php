@extends('layouts.master')

@section('style')
@endsection

@section('content')
<?php $Employee = \App\Employee::get();$Position = \App\Position::get();
    $config= \App\Config::get();
?>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-sm-12">
                <div class="card">
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                                <div class="body">
                                    <h2 class="number count-to m-t-0 m-b-5" data-from="0" data-to="{{count($Employee)}}" data-speed="2000" data-fresh-interval="700">{{count($Employee)}}</h2>
                                    <p class="text-muted">Total Pegawai</p>
                                    <span id="linecustom1">2,4,8,10,8,4,2</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 text-center">
                                <div class="body">
                                    <h2 class="number count-to m-t-0 m-b-5" data-from="0" data-to="{{count($Position)}}" data-speed="2000" data-fresh-interval="700">{{count($Position)}}</h2>
                                    <p class="text-muted ">Total Jabatan</p>
                                    <span id="linecustom2">2,4,8,10,8,4,2</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12">
                <div class="card visitors-map">
                    <div class="header">
                        <h2><strong>Menu</strong> Navigasi</h2>                   
                    </div>
                </div>
                <div class="row clearfix social-widget">                   
                    <div class="col-lg-4 col-md-6">
                        <div class="card info-box-2 hover-zoom-effect linkedin-widget waves-effect waves-blue" id="pegawai">
                            <div class="icon"><i class="material-icons">group</i></div>
                            <div class="content">
                                <div class="text">Pegawai</div>
                                <!-- <div class="number">231</div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="card info-box-2 hover-zoom-effect linkedin-widget waves-effect waves-blue" id="jabatan">
                            <div class="icon"><i class="material-icons">work</i></div>
                            <div class="content">
                                <div class="text">Jabatan</div>
                                <!-- <div class="number">2510</div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="card info-box-2 hover-zoom-effect behance-widget waves-effect waves-blue" id="penggajian">
                            <div class="icon"><i class="material-icons">account_balance_wallet</i></div>
                            <div class="content">
                                <div class="text">Penggajian</div>
                                <!-- <div class="number">121</div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="card info-box-2 hover-zoom-effect behance-widget waves-effect waves-blue" id="absensi">
                            <div class="icon"><i class="material-icons">date_range</i></div>
                            <div class="content">
                                <div class="text">Absensi</div>
                                <!-- <div class="number">121</div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="card info-box-2 hover-zoom-effect behance-widget waves-effect waves-blue" id="aktivitas">
                            <div class="icon"><i class="material-icons">history</i></div>
                            <div class="content">
                                <div class="text">Aktivitas</div>
                                <!-- <div class="number">121</div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="card info-box-2 hover-zoom-effect behance-widget waves-effect waves-blue" id="akun">
                            <div class="icon"><i class="material-icons">account_circle</i></div>
                            <div class="content">
                                <div class="text">Akun</div>
                                <!-- <div class="number">121</div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script src="/oreo/light/assets/js/pages/index.js"></script>
<script>
$(document).ready(function(){
    $('#pegawai').click(()=>{
        location.replace('/workers')
    })
    $('#jabatan').click(()=>{
        location.replace('/position')
    })
    $('#absensi').click(()=>{
        location.replace('/presence')
    })
    $('#penggajian').click(()=>{
        location.replace('/salary')
    })
    $('#aktivitas').click(()=>{
        location.replace('/workers')
    })
    $('#akun').click(()=>{
        location.replace('/profile')
    })
})
</script>
@endsection