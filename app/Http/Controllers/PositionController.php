<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PositionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['jabatan'] = \App\Position::get();
        return view('jabatan.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('jabatan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return 'success';
        // return uuid();
        $data= new \App\Position;
        $data->kode = $request->kode;
        $data->nama = $request->jabatan;
        $data->gj_pokok = str_replace(',','',$request->gaj_pokok);
        $data->gj_fungsional =str_replace(',','',$request->gj_fung);
        $data->gj_kinerja =str_replace(',','',$request->gaj_tjKinerja);
        $data->bpjs_kesehatan =str_replace(',','',$request->bpjs);
        $data->bpjs_pensiun =str_replace(',','',$request->bpjs);
        $data->bpjs_tenkerja =str_replace(',','',$request->bpjs);
        $data->save();

        return redirect('/position');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $show = \App\Position::find($id);
        $data['nama'] = $show['nama'];
        $data['kode'] = $show['kode'];
        $data['gj_pokok'] = $show['gj_pokok'];
        $data['gj_fungsional'] = $show['gj_fungsional'];
        $data['gj_kinerja'] = $show['gj_kinerja'];
        $data['bpjs_kesehatan'] = $show['bpjs_kesehatan'];
        $data['bpjs_pensiun'] = $show['bpjs_pensiun'];
        $data['bpjs_tenkerja'] = $show['bpjs_tenkerja'];
        return response($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data= \App\Position::find($id);
        $data->nama = $request->jabatan;
        $data->kode = $request->kode;
        $data->gj_pokok = $request->gaj_pokok;
        $data->gj_fungsional =$request->gj_fung;
        $data->gj_kinerja =$request->gaj_tjKinerja;
        $data->bpjs_kesehatan =$request->bpjs;
        $data->bpjs_pensiun =$request->bpjs;
        $data->bpjs_tenkerja =$request->bpjs;
        $data->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Position::find($id)->delete();
        return response('delete');
        
    }
}
