@extends('layouts.master')
@section('title')
<title>APP_Name - Employees</title>
@endsection
@section('style')

<link href="/oreo/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link href="/oreo/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<!-- Select2 -->
<link rel="stylesheet" href="/oreo/assets/plugins/select2/select2.css" />

<style>
.btn-primary-hover{
    transition: background 1s;
}
.btn-primary-hover:hover{
    background:#6572b8;
    color:white;
}
.wizard > .content{
    background :#fff;
}
.box{
    -webkit-overflow-scrolling: touch;
    overflow-x: scroll;
    overflow-y: hidden;
    white-space: nowrap;
}
.box-1{
    display:inline-block;
}
</style>
@endsection
 
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Edit</strong> Pegawai</h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else</a></li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form action="/workers/{{$id}}" method="POST" enctype="multipart/form-data">
                            @method('PUT')
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-9">
                                    <hr>
                                    <center><h6 class="text-center">Masukan Data</h6></center><hr>
                                    <div class="row clearfix">
                                        <div class="col-sm-6">
                                            <label for="">NIP</label>
                                            <div class="form-group">                                    
                                                <input type="text" value="{{$user->nip}}" class="form-control" name="nip" id="nip" placeholder="NIP" />                                   
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Nama Lengkap</label>
                                            <div class="form-group">                                    
                                                <input type="text" class="form-control" value="{{$user->nama}}"  value="" name="nama" id="nama" placeholder="Nama Lengkap" />                                   
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Tempat Lahir</label>
                                            <div class="form-group">                                    
                                                <input type="text" class="form-control" value="{{$user->tmp_lahir}}" name="tempat" id="tempat" placeholder="Tempat Lahir" />                                   
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Tanggal Lahir</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="zmdi zmdi-calendar"></i>
                                                </span>
                                                <input type="text" value="{{\Carbon\Carbon::createFromFormat('Y-m-d',$user->tgl_lahir)->format('d/m/Y')}}" class="form-control datetimepicker" value="" name="tgl" id="tgl" placeholder="Tanggal Lahir">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Jenis Kelamin</label>
                                            <select class="form-control show-tick" name="gender" id="gender">
                                                @if($user->jk == 'L')
                                                <option value="L"  selected>Laki-Laki</option>
                                                @else
                                                <option value="P" selected>Perempuan</option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Agama</label>
                                            <select class="form-control show-tick" name="agama" id="agama">
                                                @foreach($agama as $row)
                                                <option value="{{$row->id}}" @if($user->agama_id == $row->id) selected @endif>{{ucfirst($row->agama)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-12">
                                            <label for="">Alamat Lengkap</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <textarea rows="2" class="form-control no-resize" placeholder="Alamat lengkap" value="" name="alamat" id="alamat">{{$user->alamat}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Bank</label>
                                            <select class="form-control show-tick" name="bank" id="bank">
                                                <option value="BNI" @if($user->bank == 'BNI') selected @endif>Bank BNI</option>
                                                <option value="BRI" @if($user->bank == 'BRI') selected @endif> Bank BRI</option>
                                                <option value="Jateng" @if($user->bank == 'Jateng') selected @endif>Bank Jateng</option>
                                                <option value="Mandiri" @if($user->bank == 'Mandiri') selected @endif>Bank Mandiri</option>
                                                <option value="Taungan Negara" @if($user->bank == 'Taungan Negara') selected @endif>Bank Tabungan Negara</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6" class="demo-masked-input">
                                            <label for="">Nomor Rekening</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="zmdi zmdi-card"></i></span>
                                                <input type="text" class="form-control credit-card" value="{{$user->rekening}}" name="rekening" id="rekening" placeholder="Ex: 0000 0000 0000 0000">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Pendidikan</label>
                                            <select class="form-control show-tick"  name="pendidikan" id="pendidikan">
                                                <option @if($user->pendidikan == 'SMA / SMK') selected @endif value="slta">SMA / SMK</option>
                                                <option @if($user->pendidikan == 'D3') selected @endif value="d3">D3</option>
                                                <option @if($user->pendidikan == 'S1') selected @endif value="s1">S1</option>
                                                <option @if($user->pendidikan == 'S2') selected @endif value="s2">S2</option>
                                                <option @if($user->pendidikan == 'S3') selected @endif value="s3">S3</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Status</label>
                                            <select class="form-control show-tick" name="status" id="status">
                                                <option @if($user->status == 'Menikah') selected @endif value="menikah">Menikah</option>
                                                <option @if($user->status == 'Belum Menikah') selected @endif value="lajang">Belum Menikah</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-12">
                                            <label for="">Unggah Profile</label>
                                            <input type="file" class="form-control" placeholder="Unggah Profile *" value="" name="foto"  id="foto">
                                        </div>
                                        <div class="col-12">
                                            <hr>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Jabatan</label>
                                            <select class="form-control show-tick" name="jabatan" id="jabatan">
                                                @for($i=0;$i< count($jabatan); $i++)
                                                <option value="{{$jabatan[$i]->id}}" @if($jabatan[$i]->id == $user->jabatan_id) @endif>{{$jabatan[$i]->nama}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="">Tanggal Bergabung</label>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="zmdi zmdi-calendar"></i>
                                                </span>
                                                <input type="text" class="form-control datetimepicker" value="{{\Carbon\Carbon::createFromFormat('Y-m-d',$user->join)->format('d/m/Y')}}" name="join" id="join" placeholder="Tanggal bergabung">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 img-post">
                                    <center>
                                        <hr><h6>Foto</h6><hr><img class="target img-responsive" src="/uploads/{{$user->foto}}" alt="">
                                    </center>
                                </div>
                                <div class="col-md-9">
                                    <hr>
                                    <div class="row no-gutters">
                                        <div class="col-md-2 col-sm-6">
                                            <a href="/workers" class="btn btn-block btn-simple btn-round btn-secondary btCancel">Batal</a>
                                        </div>
                                        <div class="col-md-3 col-sm-6">
                                            <button class="btn btn-block btn-round btn-primary" type="submit">Perbarui Pegawai</button>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script src="/oreo/assets/plugins/momentjs/moment.js"></script> 
<!-- Jquery DataTable Plugin Js --> 
<script src="/oreo/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script> 
<script src="/oreo/assets/plugins/jquery-validation/jquery.validate.js"></script> <!-- Jquery Validation Plugin Css -->
<script src="/oreo/assets/plugins/jquery-steps/jquery.steps.js"></script> <!-- JQuery Steps Plugin Js -->
<script src="/oreo/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

<!-- <script src="/oreo/light/assets/js/pages/forms/basic-form-elements.js"></script>  -->
<script src="/oreo/light/assets/js/pages/forms/advanced-form-elements.js"></script> 
<script>
$(document).ready(function(){
    
    let _token = $('input[name="_token"]').val();

    $('.datetimepicker').bootstrapMaterialDatePicker({
        format: 'DD/MM/Y',
        clearButton: true,
        weekStart: 1,
        time: false
    });
    
    $('#btTambahPegawai').click((e)=>{

        $.ajax({
            url: url,
            method: "POST",
            data: {
                _token: _token,
                nip: $('#nip').val(),
                nama: $('#nama').val(),
                tempat: $('#tempat').val(),
                tgl: $('#tgl').val(),
                gender: $('#gender').val(),
                alamat: $('#alamat').val(),
                bank: $('#bank').val(),
                rekening: $('#rekening').val(),
                pendidikan: $('#pendidikan').val(),
                status: $('#status').val(),
                foto: $('#foto').val(),
                agama: $('#agama').val(),
            }
        })
        .done((result)=>{
            console.log(result)
        })
    })
    function showImage(src, target) {
        var fr = new FileReader();
        fr.onload = function(){
            target.src = fr.result;
            $('.target').attr('src',target.src);
        }
        fr.readAsDataURL(src.files[0]);
    }
    $('#foto').change(function putImage() {
        var src = document.getElementById("foto");
        var target = $('.target');
        showImage(src, target);
    });
})
</script>
@endsection