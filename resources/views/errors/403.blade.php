@extends('layouts.auth')
@section('content')
<?php
$config = \App\Config::first();
?>
<div class="container">
        <div class="col-md-12 content-center">
            <div class="card-plain">
                <form class="form" action="{{route('login')}}" method="POST">
                    @csrf
                    <div class="header">
                        <div class="logo-container">
                            <h3>403</h3>
                        </div>
                        <h5>Sorry Error 403</h5>
                    </div>
                    <div class="footer text-center">
                        <a href="/dashboard" class="btn btn-danger btn-round btn-lg " id="loginNow">Return Back</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
@endsection
