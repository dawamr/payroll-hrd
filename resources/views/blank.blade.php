@extends('layouts.master')
@section('title')
<title>APP_Name - Employees</title>
@endsection

@section('style')
<style>
.btn-primary-hover{
    transition: background 1s;
}
.btn-primary-hover:hover{
    background:#6572b8;
    color:white;
}
.wizard > .content{
    background :#fff;
}
.box{
    -webkit-overflow-scrolling: touch;
    overflow-x: scroll;
    overflow-y: hidden;
    white-space: nowrap;
}
.box-1{
    display:inline-block;
}
</style>
@endsection

@section('content')
    <section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>Kehadiran
                    <small>Semua Kehadiran</small>
                    </h2>
                </div>            
                @include('include.time-bar')
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix" id="">
                <div class="col-md-12">
                </div>
            </div> 
            
            <!-- #END# Advanced Form Example With Validation --> 
        </div>
    </section>
@endsection
@section('script')
@endsection