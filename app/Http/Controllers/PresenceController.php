<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PresenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function redirect($id){
        return redirect('/presence/'.$id);
    }

    public function index()
    {
        $data['absensi'] = \App\Presence::orderBy('created_at','Desc')->get();
        return view('absensi.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('absensi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        for ($i=0; $i < $request->lama ; $i++) { 
            # code...
            $presence = new \App\Presence;
            $presence->user_id = $request->id;
            $presence->keterangan = $request->ket;
            $presence->tgl = \Carbon\Carbon::createFromFormat('d/m/Y',$request->tgl)->format('Y-m-d');
            $presence->save();
        }
        return response('ok');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        
        $awalbln = new \Carbon\Carbon('first day of this month');
        $start = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$awalbln)->format('Y-m-d');
        $akhirbln = new \Carbon\Carbon('last day of this month');
        $end = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$akhirbln)->format('Y-m-d');

        $data['id'] = $id;
        $data['mulai'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$awalbln)->format('d/m/Y');
        $data['sampai'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$akhirbln)->format('d/m/Y');
        $data['user'] = \App\Employee::find($id);
        $data['sakit'] = \App\Presence::where('user_id',$id)->where('keterangan','s')->whereBetween('tgl', [$start,$end])->get();
        $data['ijin'] = \App\Presence::where('user_id',$id)->where('keterangan','i')->whereBetween('tgl', [$start,$end])->get();
        $data['alpha'] = \App\Presence::where('user_id',$id)->where('keterangan','a')->whereBetween('tgl', [$start,$end])->get();
        $data['presence'] = \App\Presence::where('user_id',$id)->whereBetween('tgl', [$start,$end])->get();
        return view('absensi.detail')->with($data);
    }

    public function showCustom(Request $request, $id){
        // dd($request); 
        $start = \Carbon\Carbon::createFromFormat('d/m/Y',$request->tgl_str)->format('Y-m-d');
        
        $end = \Carbon\Carbon::createFromFormat('d/m/Y',$request->tgl_end)->format('Y-m-d');
        
        if(empty($request->tgl_str)){
            $awalbln = new \Carbon\Carbon('first day of this month');
            $start = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$awalbln)->format('Y-m-d');
        }
        if(empty($request->tgl_end)){
            $akhirbln = new \Carbon\Carbon('last day of this month');
            $end = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$akhirbln)->format('Y-m-d');
        }
        $data['id'] = $id;
        $data['mulai'] = \Carbon\Carbon::createFromFormat('Y-m-d',$start)->format('d/m/Y');
        $data['sampai'] = \Carbon\Carbon::createFromFormat('Y-m-d',$end)->format('d/m/Y');
        $data['user'] = \App\Employee::find($id);
        $data['sakit'] = \App\Presence::where('user_id',$id)->where('keterangan','s')->whereBetween('tgl', [$start,$end])->get();
        $data['ijin'] = \App\Presence::where('user_id',$id)->where('keterangan','i')->whereBetween('tgl', [$start,$end])->get();
        $data['alpha'] = \App\Presence::where('user_id',$id)->where('keterangan','a')->whereBetween('tgl', [$start,$end])->get();
        $data['presence'] = \App\Presence::where('user_id',$id)->whereBetween('tgl', [$start,$end])->get();
        return view('absensi.detail')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}