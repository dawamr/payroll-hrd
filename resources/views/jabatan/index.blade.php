@extends('layouts.master')
@section('title')
<title>APP_Name - Employees</title>
@endsection

@section('style')
<!-- JQuery DataTable Css -->
<link rel="stylesheet" href="/oreo/assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="/oreo/assets/plugins/jquery-steps/jquery.steps.css">
<link href="/oreo/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<!-- Select2 -->
<link rel="stylesheet" href="/oreo/assets/plugins/select2/select2.css" />

<style>
.btn-primary-hover{
    transition: background 1s;
}
.btn-primary-hover:hover{
    background:#6572b8;
    color:white;
}
.wizard > .content{
    background :#fff;
}
.box{
    -webkit-overflow-scrolling: touch;
    overflow-x: scroll;
    overflow-y: hidden;
    white-space: nowrap;
}
.box-1{
    display:inline-block;
}
</style>

@endsection

@section('content')
        <div class="container-fluid">
            <div class="row clearfix" id="AppTablePosition">
                <div class="col-sm-12">
                    <div class="card">                       
                        <div class="header">
                            <h2><strong>List</strong> Posisi </h2>
                            <ul class="header-dropdown">
                                <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <ul class="dropdown-menu slideUp float-right">
                                        <li><a href="javascript:void(0);"></a></li>
                                        <li><a href="javascript:void(0);"></a></li>
                                        <li><a href="javascript:void(0);"></a></li>
                                    </ul>
                                </li>
                                <li class="remove">
                                    <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">    
                            <div class="col-3 btn-primary-hover waves-effect" id="btTambahPosisi">
                                <button class="btn btn-primary btn-icon btn-round hidden-sm-down m-l-10" type="button">
                                    <i class="zmdi zmdi-plus"></i>
                                </button> Tambah Posisi
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Jabatan</th>
                                            <th>Gaji Pokok</th>
                                            <th>Tunjangan Fungsional</th>
                                            <th>Tunjangan Kinerja</th>
                                            <th>BPJS Kesehatan</th>
                                            <th>BPJS Ketenagakerjaan</th>
                                            <th>BPJS Pensiun</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Kode</th>
                                            <th>Jabatan</th>
                                            <th>Gaji Pokok</th>
                                            <th>Tunjangan Fungsional</th>
                                            <th>Tunjangan Kinerja</th>
                                            <th>BPJS Kesehatan</th>
                                            <th>BPJS Ketenagakerjaan</th>
                                            <th>BPJS Pensiun</th>
                                            <th>Hapus</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @foreach($jabatan as $data)
                                        <tr>
                                            <td>{{$data->kode}}</td>
                                            <td><a href="#" url="/position/{{$data->id}}" class="tdPosisi">{{$data->nama}}</a></td>
                                            <td>{{number_format($data->gj_pokok)}},-</td>
                                            <td>{{number_format($data->gj_fungsional)}},-</td>
                                            <td>{{number_format($data->gj_kinerja)}},-</td>
                                            <td>Rp. {{number_format($data->bpjs_kesehatan)}},-</td>
                                            <td>Rp. {{number_format($data->bpjs_pensiun)}},-</td>
                                            <td>Rp. {{number_format($data->bpjs_tenkerja)}},-</td>
                                            <td class="text-center">
                                                <a href="#" url="/position/{{$data->id}}" class="waves-effect inline-block tdHapus"><i class="fas fa-trash"></i> </a>
                                            </td>
                                        </tr>
                                        <script>
                                            $('#open{{$data->id}}').click(()=>{
                                                $('#AppTablePosition').fadeOut();
                                                // $('#AppCreatePosition').fadeOut();
                                                $('#AppEditPosition').fadeIn(1600);
                                                // alert('hai')
                                            })
                                        </script>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
        <div class="container-fluid">
            <div class="row clearfix" id="AppCreatePosition">
                <div class="col-sm-12">
                    <div class="card">                       
                        <div class="header">
                            <h2><strong>Posisi</strong> Baru </h2>
                            <ul class="header-dropdown">
                                <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <ul class="dropdown-menu slideUp float-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else</a></li>
                                    </ul>
                                </li>
                                <li class="remove">
                                    <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="body" style="margin-left:1em;margin-right:1em">
                            
                            <form>
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="form-group col-md-4 form-float"><span>Kode Jabatan</span>
                                        <input type="text" class="form-control" placeholder="Kode Jabatan *" name="kode"  id="frkode" required>
                                    </div>
                                    <div class="form-group col-md-8 form-float"><span>Jabatan</span>
                                        <input type="text" class="form-control" placeholder="Jabatan *" name="jabatan"  id="fr1" required>
                                    </div>
                                    <div class="form-group col-md-4 form-float"><span>Gaji Pokok</span>
                                        <input type="text" class="form-control money gaji" placeholder="Rp. *" name="gjPokok"  id="fr2" required>
                                    </div>
                                    <div class="form-group col-md-4 form-float"><span>Tunjangan Fungsional</span>
                                        <input type="text" class="form-control money gaji" placeholder="Rp. *" name="gjFungsional"  id="fr3" required>
                                    </div>
                                    <div class="form-group col-md-4 form-float"><span>Tunjangan Kinerja</span>
                                        <input type="text" class="form-control money " placeholder="Rp. *" name="gjKinerja"  id="fr4" required>
                                    </div>
                                    <hr>
                                </div>
                            </form>
                        </div>
                        <div class="footer">
                            <div class="form-group" style="margin-left:1.5em">
                                <!-- <button class="btn btn-danger btn-round waves-effect" id="btDelete" index=""> Hapus </button> -->
                                <a href="" id="btBatalTambah" class="btn btn-secondary btn-round">Batal</a>
                                <button class="btn btn-info btn-round" id="btSimpanTambah" type="submit"> Simpan </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
        <div class="container-fluid">
            <div class="row clearfix" id="AppEditPosition">
                <div class="col-sm-12">
                    <div class="card">                       
                        <div class="header">
                            <h2><strong>Edit</strong> Posisi <span class="nama_posisi"></span> </h2>
                            <ul class="header-dropdown">
                                <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <ul class="dropdown-menu slideUp float-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else</a></li>
                                    </ul>
                                </li>
                                <li class="remove">
                                    <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="body" style="margin-left:1em;margin-right:1em">
                            
                            <form>
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="form-group col-md-4 form-float"><span>Kode Jabatan</span>
                                        <input type="text" class="form-control" placeholder="Kode Jabatan *" name="kode"  id="edKode" required>
                                    </div>
                                    <div class="form-group col-md-8 form-float"><span>Jabatan</span>
                                        <input type="text" class="form-control" placeholder="Jabatan *" name="jabatan"  id="edJabatan" required>
                                        <input type="hidden" name="id" id="frid">
                                    </div>
                                    <div class="form-group col-md-4 form-float"><span>Gaji Pokok</span>
                                        <input type="text" class="form-control money gaji" placeholder="Rp. *" name="gjPokok"  id="edGapok" required>
                                    </div>
                                    <div class="form-group col-md-4 form-float"><span>Tunjangan Fungsional</span>
                                        <input type="text" class="form-control money gaji" placeholder="Rp. *" name="gjFungsional"  id="edGafung" required>
                                    </div>
                                    <div class="form-group col-md-4 form-float"><span>Tunjangan Kinerja</span>
                                        <input type="text" class="form-control money " placeholder="Rp. *" name="gjKinerja" value="0" id="edGakin" required>
                                    </div>
                                    <hr>
                                </div>
                            </form>
                        </div>
                        <div class="footer">
                            <div class="form-group" style="margin-left:1.5em">
                                <a href="#" id="btBatalEdit" class="btn btn-secondary btn-round">Batal</a>
                                <button class="btn btn-info btn-round" id="btSimpanEdit" type="submit"> Simpan </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
@endsection
@section('script')
<!-- Large Size -->
<!-- Jquery DataTable Plugin Js --> 
<script src="/oreo/light/assets/bundles/datatablescripts.bundle.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.print.min.js"></script>

<script src="/oreo/assets/plugins/jquery-validation/jquery.validate.js"></script> <!-- Jquery Validation Plugin Css -->
<script src="/oreo/assets/plugins/jquery-steps/jquery.steps.js"></script> <!-- JQuery Steps Plugin Js -->
<script src="/js/simple.money.format.js"></script>
<script>
$('.money').simpleMoneyFormat();
</script>
<script src="/oreo/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> <!-- Input Mask Plugin Js --> 
<script src="/oreo/assets/plugins/select2/select2.min.js"></script> <!-- Select2 Js -->
<script>
    $(document).ready(function(){
        $('#AppCreatePosition').hide();
        $('#AppEditPosition').hide();
        $('#AppTablePosition').fadeIn();
        ///////////////////////////////\
        $('[data-toggle="tooltip"]').tooltip()
        ////////////////////////////////
        let _token = $('input[name="_token"]').val();
        $('.tdPosisi').click(function(e){
            e.preventDefault();
            $('#AppEditPosition').fadeIn(1600);
            $('#AppTablePosition').fadeOut();
            let url = $(this).attr('url');
            // let simpan  ={
            //         jabatan :$('#edJabatan').val(),
            //         gj_pokok: $('#edGapok').val(),
            //         gj_fungsional:$('#edGafung').val(),
            //         gj_kinerja:$('#edGakin').val(),
            //         bpjs_kesehatan:$('#edBkes').val(),
            //         bpjs_tenkerja:$('#edBker').val(),
            //         bpjs_pensiun:$('#edBPen').val(),
            //     }
            $.ajax({
                url :url,
                method : "GET",
                dataType : 'json',
                encode : true
            }).done((data)=>{
                $('#edJabatan').val(data.nama),
                $('#edKode').val(data.kode),
                $('#edGapok').val(data.gj_pokok),
                $('#edGafung').val(data.gj_fungsional),
                $('#edGakin').val(data.gj_kinerja),
                $('#edBkes').val(data.bpjs_tenkerja),
                $('#edBker').val(data.bpjs_kesehatan),
                $('#edBPen').val(data.bpjs_pensiun)
            });
        });
        $('.tdHapus').click(function(e){
            e.preventDefault();
            let url = $(this).attr('url');
            $.ajax({
                url: url,
                method : "DELETE",
                data :{
                    _token:_token
                }, success:function(result){
                    location.reload();
                }                      
            });
        });

        $('#btBatalTambah').click((e)=>{
            e.preventDefault();
            $('#AppTablePosition').fadeIn(1600);
            $('#AppCreatePosition').fadeOut();
        })
        $('#btBatalEdit').click((e)=>{
            e.preventDefault();
            $('#AppTablePosition').fadeIn(1600);
            $('#AppEditPosition').fadeOut();
        })
        $('#btTambahPosisi').click(()=>{
            $('#AppCreatePosition').fadeIn(1600);
            $('#AppTablePosition').fadeOut();
        });

        $('#btSimpanTambah').click(()=>{
           if($('#frid').val().length >0){
                $.ajax({
                    url :"/position/"+ $('#frid').val()+"",
                    method : "PUT",
                    data :{
                        _token:_token,
                        kode: $('#frkode'),
                        jabatan: $('#fr1'),
                        gaj_pokok:$('#fr2'),
                        gj_fung:$('#fr3'),
                        gaj_tjKinerja:$('#fr4'),
                        bpjs : $('#fr5')
                        
                    }, success:function(result){
                        location.reload();
                    }                      
                });
           }else{
                $.ajax({
                    url :"/position",
                    method : "POST",
                    data :{
                        _token:_token,
                        kode: $('#frkode'),
                        jabatan: $('#fr1'),
                        gaj_pokok:$('#fr2'),
                        gj_fung:$('#fr3'),
                        gaj_tjKinerja:$('#fr4'),
                        bpjs : $('#fr5')
                        
                    }, success:function(result){
                        location.reload();
                    }                      
                });
           }
             
        });


        ////////////////////////////////
        $('.gaji').eq(0).val(0);$('.gaji').eq(1).val(0);
        $('.gaji').eq(0).keyup(function(){
            var nilai = (parseInt($(this).val().split(',').join('')) + parseInt($('.gaji').eq(1).val().split(',').join('')))/100;
            $('.bpjs').eq(0).val(nilai);
            $('.bpjs').eq(1).val(nilai);
            $('.bpjs').eq(2).val(nilai);
        });
        $('.gaji').eq(1).keyup(function(){
            var nilai = (parseInt($(this).val().split(',').join('')) + parseInt($('.gaji').eq(0).val().split(',').join('')))/100;
            $('.bpjs').eq(0).val(nilai);
            $('.bpjs').eq(1).val(nilai);
            $('.bpjs').eq(2).val(nilai)
            // console.log(nilai)
        });
        $('.date').inputmask('dd/mm/yyyy', { placeholder: '__/__/____' , inputFormat: "dd/mm/yyyy"});
        // $('#jabatan').select2();

    });

</script>
<!-- Bootstrap Material Datetime Picker Plugin Js --> 
<!-- <script src="/oreo/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>  -->

<script src="/oreo/light/assets/js/pages/tables/jquery-datatable.js"></script>
@endsection