<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/','ConfigController@index');
Route::get('/dashboard','HomeController@index');

Route::resources(['middleware' => 'auth',
    'workers' => 'EmployeeController',
    'presence' => 'PresenceController',
    'position' => 'PositionController',
    'salary' => 'SalaryController',
    'activity' => 'ActivityController',
]);
Route::group(['middleware' => ['verified']], function () {
});

route::post('/presence/s/{id}','PresenceController@showCustom');
route::get('/presence/s/{id}','PresenceController@redirect');

Route::get('/api/byNip/{nip}','ApiController@byNip');

Route::get('/api/presence','ApiController@presence');

Route::get('/get/slip-gaji','DownloadController@SlipGaji');

Route::get('/logout','HomeController@logout');

Route::get('/setup','ConfigController@index');
Route::post('/setup','ConfigController@configSetup');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
