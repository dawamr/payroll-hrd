@extends('layouts.master')
@section('title')
<title>APP_Name - Employees</title>
@endsection

@section('style')
<!-- JQuery DataTable Css -->
<link rel="stylesheet" href="/oreo/assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css">
<link href="/oreo/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<!-- Select2 -->
<link rel="stylesheet" href="/oreo/assets/plugins/select2/select2.css" />

<style>
.btn-primary-hover{
    transition: background 1s;
}
.btn-primary-hover:hover{
    background:#6572b8;
    color:white;
}
.wizard > .content{
    background :#fff;
}
.box{
    -webkit-overflow-scrolling: touch;
    overflow-x: scroll;
    overflow-y: hidden;
    white-space: nowrap;
}
.box-1{
    display:inline-block;
}
</style>

@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix" id="AppTablePosition">
            <div class="col-sm-12">
                <div class="card">                       
                    <div class="header">
                        <h2><strong>List</strong> Posisi </h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu slideUp float-right">
                                    <li><a href="javascript:void(0);"></a></li>
                                    <li><a href="javascript:void(0);"></a></li>
                                    <li><a href="javascript:void(0);"></a></li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">    
                        <div class="col-3 btn-primary-hover waves-effect" id="btTambahPosisi">
                            <button class="btn btn-primary btn-icon btn-round hidden-sm-down m-l-10" type="button">
                                <i class="zmdi zmdi-plus"></i>
                            </button> Tambah Posisi
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Aktivitas</th>
                                        <th>oleh</th>
                                        <th>Tanggal</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Aktivitas</th>
                                        <th>oleh</th>
                                        <th>Tanggal</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach($activity as $data)
                                        <?php $user = \App\User::find($data->user_id) ?>
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$data->keterangan}}</td>
                                            <td>{{$user['name']}}</td>
                                            <td>{{$data->created_at}}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
@endsection
@section('script')
<!-- Large Size -->
<!-- Jquery DataTable Plugin Js --> 
<script src="/oreo/light/assets/bundles/datatablescripts.bundle.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.print.min.js"></script>

<script>
    $(document).ready(function(){

    });
</script>
<!-- Bootstrap Material Datetime Picker Plugin Js --> 
<!-- <script src="/oreo/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>  -->

<script src="/oreo/light/assets/js/pages/tables/jquery-datatable.js"></script>
@endsection