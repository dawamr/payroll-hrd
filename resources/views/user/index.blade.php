@extends('layouts.master')
@section('title')
<title>APP_Name - Employees</title>
@endsection

@section('style')
<style>
.btn-primary-hover{
    transition: background 1s;
}
.btn-primary-hover:hover{
    background:#6572b8;
    color:white;
}
.wizard > .content{
    background :#fff;
}
.box{
    -webkit-overflow-scrolling: touch;
    overflow-x: scroll;
    overflow-y: hidden;
    white-space: nowrap;
}
.box-1{
    display:inline-block;
}
</style>
@endsection

@section('content')
    <section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2>Kehadiran
                    <small>Semua Kehadiran</small>
                    </h2>
                </div>            
                @include('include.time-bar')
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix" id="">
                <div class="col-md-12">
                    <div class="card">
                        <div class="body">
                            <div class="row">
                                <div class="col-md-4 text-center">
                                    <div class="profile-image">
                                        <img src="/oreo/assets/images/profile_av.jpg" alt="" style="border-radius:50%"  class="img-responsive mt-4"><hr>
                                    </div> 
                                    <h5>Nama</h5>
                                    <b>Position</b>
                                    <hr>
                                    <div class="post-toolbar-b">
                                        <button id="btCancel" data-toggle="tooltip" data-placement="bottom" title="Kembali" class="btn btn-secondary btn-icon  btn-icon-mini btn-round"><i class="material-icons">chevron_left</i></button>
                                        <button id="btEdit" url="" data-toggle="tooltip" data-placement="bottom" title="Edit" class="btn btn-warning btn-icon  btn-icon-mini btn-round"><i class="material-icons">mode_edit</i></button>
                                        <form action="" id="frDelete" method="post">
                                        @method('DELETE')
                                        {{csrf_field()}}
                                            <button type="submit" data-toggle="tooltip" data-placement="bottom" title="Hapus" class="btn btn-danger btn-icon  btn-icon-mini btn-round"><i class="material-icons">delete_forever</i></button>
                                        </form>
                                    </div>
                                    <hr>
                                </div>
                                <div class="col-md-4">
                                    <small class="text-muted">NIP: </small>
                                    <p id="nip"></p>
                                    <hr>
                                    <small class="text-muted">Nama Lengkap: </small>
                                    <p id="fullname"></p>
                                    <hr>
                                    <small class="text-muted">Tempat Lahir: </small>
                                    <p id="tmp_lahir"></p>
                                    <hr>
                                    <small class="text-muted">Tanggal Lahir: </small>
                                    <p id="tgl_lahir" class="m-b-0"></p>
                                    <hr>
                                    <small class="text-muted">Jenis Kelamin: </small>
                                    <p id="jk"></p>
                                    <hr>
                                    <small class="text-muted">Agama: </small>
                                    <p id="agama"></p>
                                    <hr>
                                    <small class="text-muted">Alamat: </small>
                                    <p id="alamat"></p>
                                    <hr>
                                    <small class="text-muted">Jabatan: </small>
                                    <p id="position" class="m-b-0"></p>
                                </div>
                                <div class="col-md-4">
                                    <small class="text-muted">Tanggal Bergabung: </small>
                                    <p id="tgl_bergabung"></p>
                                    <hr>
                                    <small class="text-muted">Pendidikan Terakhir: </small>
                                    <p id="pendidikan"></p>
                                    <hr>
                                    <small class="text-muted">Nomor Rekening: </small>
                                    <p id="no_rek"></p>
                                    <hr>
                                    <small class="text-muted">Bank: </small>
                                    <p>Bank <span id="bank"></span></p>
                                    <hr>
                                    <small class="text-muted">Status Perkawinan: </small>
                                    <p id="status"></p>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            
            <!-- #END# Advanced Form Example With Validation --> 
        </div>
    </section>
@endsection
@section('script')
@endsection