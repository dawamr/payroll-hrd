<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Uuid;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Crypt;


class ConfigController extends Controller
{
    public function index(){
        $cek = \App\Config::first();
        if(!isset($cek) && $cek == null){
            return view('auth.setup')->with('config',$cek);
        }else{
            return redirect('/dashboard');
        }
    }

    public function configSetup(Request $request){
        $cek = \App\Config::first();
        if(!isset($cek) && $cek == null){
            $uuid = Uuid::generate(1);
            $data = new \App\Config;
            $data->nama_aplikasi = $request->appName;
            $data->email_gmail = $request->appEmail;
            $data->password_gmail = Crypt::encryptString(10,$request->appPassword);
            $data->password_aplikasi = Crypt::encryptString(10,$request->appPassword2);
            if(Input::File('foto')){
                $file   = Input::file('foto');
                $file_name = $uuid.".".$file->getClientOriginalExtension();
                $path = public_path('uploads/' .$file_name);
                $img = \Image::make($file->getRealPath());
                $img->resize(null, 700, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save($path);
                // $profil->photo =  $file_name;
                $file->move('uploads',  $file_name);
                $data->logo_aplikasi = $file_name;
            }
            $user= new \App\User;
            $user->name = 'Admin';
            $user->email = $request->appEmail;
            $user->password = bcrypt($request->password);
            $data->save();
            $user->save();
            
            $ac = new \App\Activity;
            $ac->user_id = $user->id;
            $ac->keterangan = 'Setup aplikasi';
            $ac->save();
            return redirect('/');
        }else{
            return redirect('/dashboard');
        }
        // dd($request);
    }
}
