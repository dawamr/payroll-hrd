@php
$sekarang = \Carbon\Carbon::Now()->format('l');
if($sekarang == 'Saturday' ){$sekarang = 'Sabtu';}
if($sekarang == 'Sunday'){$sekarang = 'Minggu';}
if($sekarang == 'Monday'){$sekarang = 'Senin';}
if($sekarang == 'Tuesday'){$sekarang = 'Selasa';}
if($sekarang == 'Wednesday'){$sekarang = 'Rabu';}
if($sekarang == 'Thursday'){$sekarang = 'Kamis';}
if($sekarang == 'Friday'){$sekarang = 'Jumat';}
if(date('H') >=5 && date('H') <=10){
    $data = array("Selamat Pagi","Semangat Pagi","Tetap Semangat");
    $waktu=array_rand($data,1);
}
if(date('H')>=11 && date('H') <=14){
    $data = array("Selamat Siang","Semangat Siang","Tetap Semangat");
    $waktu=array_rand($data,1);
}
if(date('H')>=15 && date('H') <=17){
    $data = array("Selamat Sore","Semangat Sore","Tetap Semangat");
    $waktu=array_rand($data,1);
}
if(date('H')>=18 && date('H') <=24){
    $data = array("Selamat Malam","Semangat Malam","Tetap Semangat");
    $waktu=array_rand($data,1);
}
if(date('H')>=0 && date('H') <=4){
    $data = array("Ayo Puasa","Shalat Tahajud","Selamat Malam");
    $waktu=array_rand($data,1);
}
$config = \App\Config::get();
@endphp
<div class="col-lg-7 col-md-7 col-sm-12 text-right">
    <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
    <h2 class="number count-to m-t-0 m-b-5" data-from="00.00" data-to="23.59" data-speed="1000" data-fresh-interval="700"><blink>{{\Carbon\Carbon::Now()->format('h:i')}}</blink></h2>
        <small class="col-white">
        <!-- {{$data[$waktu]}} -->
        <br>
        </small>
    </div>
    <div class="inlineblock text-center m-r-15 m-l-15 hidden-md-down">
    <h2 class="number count-to m-t-0 m-b-5" data-from="01" data-to="31" data-speed="1000" data-fresh-interval="700">{{$sekarang}}, {{\Carbon\Carbon::Now()->format('d-m-Y')}}</h2>
        <small class="col-white">Semarang, Jawa Tengah</small>
    </div>
    
</div>