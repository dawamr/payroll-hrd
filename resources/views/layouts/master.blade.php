<?php 
    $config = \App\Config::get();
?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Aplikasi Penggajian - payroll">
<title>@if(!isset($config)) {{$config[0]->nama_aplikasi}} - @endif  {{Date('Y')}}</title>
<link rel="icon" href="favicon.ico" type="image/x-icon"> <!-- Favicon-->
<link rel="stylesheet" href="/oreo/assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/oreo/assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
<link rel="stylesheet" href="/oreo/assets/plugins/morrisjs/morris.css" />
<!-- Custom Css -->
<link rel="stylesheet" href="/oreo/light/assets/css/main.css">
<link rel="stylesheet" href="/oreo/light/assets/css/color_skins.css">
<link href="/fa/css/fontawesome.css" rel="stylesheet">
<link rel="stylesheet" href="/fa/css/all.min.css">
<script src="/fa/js/all.min.js"></script> 
<style>
  .form-control{
    color :#0a0b0c;
  }
  option{
    color :#0a0b0c !important;
  }
  #leftsidebar{
    overflow-x: hidden;
    overflow-y: scroll;
  }
  blink {
        animation: blinker 0.6s linear infinite;
        color: #1c87c9;
       }
      @keyframes blinker {  
        50% { opacity: 0; }
       }
       .blink-one {
         animation: blinker-one 1s linear infinite;
       }
       @keyframes blinker-one {  
         0% { opacity: 0; }
       }
       .blink-two {
         animation: blinker-two 1.4s linear infinite;
       }
       @keyframes blinker-two {  
         100% { opacity: 0; }
       }
</style>
<script
  src="/js/jquery.min.js"></script>
@yield('style')

</head>
<?php
$theme = 'blue';
if(date('H') >=5 && date('H') <=10){
    $theme = 'orange';
}
if(date('H')>=11 && date('H') <=14){
    $theme = 'cyan';
}
if(date('H')>=15 && date('H') <=17){
    $theme = 'blush';
}
if(date('H')>=18 && date('H') <=24){
    $theme = 'purple';
}
if(date('H')>=0 && date('H') <=4){
    $theme = 'purple';
}
?>
<body class="theme-{{$theme}}">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="{{asset('icon/loading.png')}}" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<!-- Top Bar -->
@include('include.top-navbar')

<!-- Left Sidebar -->
@include('include.left-sidebar')

<!-- Right Sidebar -->
@include('include.right-sidebar')

<!-- Chat-launcher -->
@include('include.chat-launcher')

<!-- Main Content -->
<section class="content home">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <h2> <marquee width="100%" direction="right"> Selamat Datang </marquee>
                    <small>@if(!isset($config)) {{$config[0]->nama_aplikasi}} {{Date('Y')}} @endif</small>
                    </h2>
                </div>            
                @include('include.time-bar')
            </div>
        </div>
        @yield('content')
    </section>

<!-- Jquery Core Js --> 
<script src="/oreo/light/assets/bundles/libscripts.bundle.js"></script> 
<script src="/oreo/light/assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->

<script src="/oreo/light/assets/bundles/jvectormap.bundle.js"></script> <!-- JVectorMap Plugin Js -->
<script src="/oreo/light/assets/bundles/morrisscripts.bundle.js"></script>

@yield('script')

<script src="/oreo/light/assets/bundles/mainscripts.bundle.js"></script>
</body>

</html>