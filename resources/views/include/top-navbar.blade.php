<nav class="navbar p-l-5 p-r-5">
    <ul class="nav navbar-nav navbar-left">
        <li>
            <div class="navbar-header">
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" id="appName" href="#">
                <img src="{{asset('icon/nawakara.jpg')}}" width="30" alt="Oreo"><span class="m-l-10">
                {{$config[0]->nama_aplikasi}}</span></a>
            </div>
        </li>        
        <li class="hidden-sm-down">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-addon"><i class="zmdi zmdi-search"></i></span>
            </div>
        </li>        
        <li class="float-right">
            <a href="javascript:void(0);" title="Zoom" class="ls-toggle-btn" data-close="true"><i class="zmdi zmdi-swap"></i></a>
            <a href="/logout" class="mega-menu" title="Logout" data-close="true"><i class="zmdi zmdi-power"></i></a>
            <!-- <a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a> -->
        </li>
    </ul>
</nav>