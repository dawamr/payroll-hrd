@extends('layouts.master')
@section('title')
<title>APP_Name - Employees</title>
@endsection

@section('style')
<!-- JQuery DataTable Css -->
<link rel="stylesheet" href="/oreo/assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="/oreo/assets/plugins/jquery-steps/jquery.steps.css">
<link href="/oreo/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

<link href="/oreo/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<!-- Select2 -->
<link rel="stylesheet" href="/oreo/assets/plugins/select2/select2.css" />

<style>
.btn-primary-hover{
    transition: background 1s;
}
.btn-primary-hover:hover{
    background:#6572b8;
    color:white;
}
.wizard > .content{
    background :#fff;
}
.box{
    -webkit-overflow-scrolling: touch;
    overflow-x: scroll;
    overflow-y: hidden;
    white-space: nowrap;
}
.box-1{
    display:inline-block;
}
</style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row clearfix" id="AppTablePresence">
            <div class="col-sm-12">
                <div class="card">                       
                    <div class="header">
                        <h2><strong>List</strong> Kehadiran </h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu slideUp float-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else</a></li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                    <?php
                        $bulan = \Carbon\Carbon::Now()->format('m');
                        $nmBulan = array(1=>'Januari',2=>'Febuari',3=>'Maret',4=>'April',5=>'Mei',6=>'Juni',7=>'Juli',8=>'Agustus',9=>'September',10=>'Oktober',11=>'November',12=>'Desember');
                        for($i=1;$i<13;$i++)
                            if($bulan == $i){$bulan = $nmBulan[$i];}
                    ?>
                        <h4 class="text-center">Absensi Bulan {{$bulan}}</h4>
                        <div class="col-3 btn-primary-hover waves-effect" data-toggle="modal" data-target="#ModalTambahAbsensi">
                            <button class="btn btn-primary btn-icon btn-round hidden-sm-down m-l-10" type="button">
                                <i class="zmdi zmdi-plus"></i>
                            </button> Input Kehadiran   
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Nip</th>
                                            <th>Nama</th>
                                            <th>Jabatan</th>
                                            <th>Sakit</th>
                                            <th>Ijin</th>
                                            <th>Alpha</th>
                                            <th>Detail</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Nip</th>
                                            <th>Nama</th>
                                            <th>Jabatan</th>
                                            <th>Sakit</th>
                                            <th>Ijin</th>
                                            <th>Alpha</th>
                                            <th>Detail</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <?php 
                                            $pegawai = \App\Employee::get();
                                        ?>
                                        @for($i=0;$i< count($pegawai);$i++)
                                        <?php
                                            $startTh = new \Carbon\Carbon('first day of January');
                                            $startTh = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$startTh)->format('Y-m-d');
                                            $endTh = new \Carbon\Carbon('last day of December');
                                            $endTh = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$endTh)->format('Y-m-d');

                                            $start = new \Carbon\Carbon('first day of this month');
                                            $start = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$start)->format('Y-m-d');
                                            $end = new \Carbon\Carbon('last day of this month');
                                            $end = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$end)->format('Y-m-d');
                                            $jab = \App\Position::find($pegawai[$i]->jabatan_id); 
                                            $sakit = \App\Presence::where('user_id',$pegawai[$i]->id)->where('keterangan','s')->whereBetween('tgl', [$start,$end])->get();
                                            $ijin = \App\Presence::where('user_id',$pegawai[$i]->id)->where('keterangan','i')->whereBetween('tgl', [$start,$end])->get();
                                            $alpha = \App\Presence::where('user_id',$pegawai[$i]->id)->where('keterangan','a')->whereBetween('tgl', [$start,$end])->get();
                                        ?>
                                        <tr>
                                            <td>{{$pegawai[$i]->nip}}</td>
                                            <td>{{$pegawai[$i]->nama}}</td>
                                            <td>{{$jab->nama}}</td>
                                            <td>{{count($sakit)}}</td>
                                            <td>{{count($ijin)}}</td>
                                            <td>{{count($alpha)}}</td>
                                            <td class="text-center" ><a href="/presence/{{$pegawai[$i]->id}}" ><i class="fas fa-external-link-alt"></i></a></td>
                                        </tr>
                                        @endfor
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="card">
                    <div class="body">
                        <div class="container">
                            <h4 class="text-center">Absensi Terbaru</h4>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>NIP</th>
                                            <th>Nama</th>
                                            <th>Keterangan</th>
                                            <th>Taggal</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                        <th>#</th>
                                            <th>NIP</th>
                                            <th>Nama</th>
                                            <th>Keterangan</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        @php $no =1 @endphp
                                        @foreach($absensi as $row)
                                        <?php 
                                            $nama = \App\Employee::find($row->user_id);
                                            $jab = \App\Position::find($nama['jabatan_id']);
                                        ?>
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$nama['nip']}}</td>
                                            <td>{{$nama['nama']}}</td>
                                            <td>
                                                @if($row->keterangan == 's') Sakit @endif
                                                @if($row->keterangan == 'i') Ijin @endif
                                                @if($row->keterangan == 'a') Alpha @endif
                                            </td>
                                            <td>{{\Carbon\Carbon::createFromFormat('Y-m-d', $row->tgl)->format('d/m/Y')}}</td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        
        <!-- #END# Advanced Form Example With Validation --> 
    </div>
@endsection
@section('script')
<!-- Large Size -->
<div class="modal fade" id="ModalTambahAbsensi" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="title" id="largeModalLabel">Ijin Pegawai</h4>
            </div>
            <form class="form">
                {{csrf_field()}}
                <div class="modal-body"> 
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group col-md-12 form-float"><span>NIP</span>
                                <input type="text" class="form-control select2" placeholder="NIP *" name="nip"  id="inputNip" required>
                            </div>
                            <div class="form-group col-md-12 form-float"><span>Keterangan</span>
                                <select class="form-control" name="keterangan" id="inputKet">
                                    <option value="s">Sakit</option>
                                    <option value="i">Ijin</option>
                                    <option value="a">Alpha</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12 form-float"><span>Tanggal</span>
                                <input type="text" class="form-control datetimepicker" placeholder="Tanggal" id="inputTgl" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group col-md-12 form-float"><span>Nama</span>
                                <input type="text" class="form-control" placeholder="Nama *" name="nama"  id="inputNama" required>
                            </div>
                            <div class="form-group col-md-12 form-float"><span>Jabatan</span>
                                <input type="text" class="form-control" placeholder="Jabatan *" name="jabatan"  id="inputJab" required>
                            </div>
                            <div class="col-md-12">
                                <img id="profile" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer ml-3">
                    <div class="form-group pull-right pl-2 pr-2">
                        <button class="btn btn-simple btn-round waves-effect" data-dismiss="modal">Batal</button>
                        <button class="btn btn-info btn-round waves-effect" type="submit" id="simpanData"> Simpan </button>
                    </div>
                    {{-- <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button> --}}
                </div>
            </form>
            
            
        </div>
    </div>
</div>

<script src="/oreo/assets/plugins/momentjs/moment.js"></script> 
<!-- Jquery DataTable Plugin Js --> 
<script src="/oreo/light/assets/bundles/datatablescripts.bundle.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="/oreo/assets/plugins/jquery-datatable/buttons/buttons.print.min.js"></script>
<script src="/oreo/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script> 
<script src="/oreo/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> <!-- Input Mask Plugin Js --> 
<script src="/oreo/assets/plugins/select2/select2.min.js"></script> <!-- Select2 Js -->
<script>
    $(document).ready(function(){
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
        ////////////////////////////////
        
        $('.datetimepicker').bootstrapMaterialDatePicker({
            format: 'DD/MM/Y',
            clearButton: true,
            weekStart: 1,
            time: false
        });
        var dd;
        $('#inputNip').keyup((e)=>{
            e.preventDefault();
            $.ajax({
                url :'/api/byNip/'+$('#inputNip').val()+'' ,
                method : "GET",
                dataType : 'json',
                encode : true
            }).done((data)=>{
                console.log(data.nama);
                if(data.length >0){
                    $('#inputNama').val(data[0].nama);
                    $('#inputJab').val(data[0].jabatan);
                    $('#profile').attr('scr', '/uploads/'+data[0].foto)
                    $('#inputTgl').val(<?php Date('dmY') ?>);
                    dd = data[0].id;
                }else{
                    $('#inputNama').val('');
                    $('#inputJab').val('');
                    $('#inputTgl').val('');
                }
            })  
        });
        $('#simpanData').click((e)=>{
            e.preventDefault();
            if($('#inputNama').val().length =0){return 0}
            if($('#inputJab').val().length =0){return 0}
            if($('#inputTgl').val().length =0){return 0}
            if($('#inputKet').val().length =0){return 0}
            $.ajax({
                url :'/presence' ,
                method : "POST",
                data: {
                    _token : $('input[name="_token"]').val(),
                    nama:$('#inputNama').val(),
                    id:dd,
                    jabatan: $('#inputJab').val(),
                    tgl: $('#inputTgl').val(),
                    ket: $('#inputKet').val()
                }
            }).done((data)=>{
                location.reload();
            })
        });
            
        
        // $('#inputNip').select2((data)=>{
        //     allowClear: true,
        //     multiple: true,
        //     maximumSelectionSize: 1,
        //     placeholder: "Start typing",
        //     data: data    
        // });
    });

</script>
<!-- Bootstrap Material Datetime Picker Plugin Js --> 
<!-- <script src="/oreo/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>  -->

<script src="/oreo/light/assets/js/pages/tables/jquery-datatable.js"></script>
@endsection