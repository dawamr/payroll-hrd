<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $new = new \App\User;
        $new->name = 'ADMIN';
        $new->email = 'admin@nawakara.com';
        $new->password = bcrypt('0');
        $new->tmp_lahir = 'Semarang';
        $new->tgl_lahir = '2000-12-14';
        $new->jk = 'L';
        $new->alamat = 'Jl jalan';
        $new->pendidikan =  'S2';
        $new->role = 'admin';
        $new->save();

        $new = new \App\User;
        $new->name = 'STAF';
        $new->email = 'staf@nawakara.com';
        $new->password = bcrypt('0');
        $new->tmp_lahir = 'Semarang';
        $new->tgl_lahir = '2000-12-14';
        $new->jk = 'L';
        $new->alamat = 'Jl jalan';
        $new->pendidikan =  'S2';
        $new->role = 'staf';
        $new->save();
    }
}
