<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Uuid;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;


class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pegawai'] = \App\Employee::get();
        $data['position'] = \App\Position::select('id','nama')->get();
        $data['agama'] = \App\Region::get();
        return view('pegawai.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['agama'] = \App\Region::orderBy('agama','Asc')->get();
        $data['jabatan'] = \App\Position::orderBy('nama','Asc')->get();
        return view('pegawai.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $uuid = Uuid::generate(1);
        $file_name = '';
        $data = new \App\Employee;
        if(Input::File('foto')){
            $file   = Input::file('foto');
            $file_name = $uuid.".".$file->getClientOriginalExtension();
            $path = public_path('uploads/' .$file_name);
            $img = \Image::make($file->getRealPath());
            $img->resize(null, 700, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($path);
            // $profil->photo =  $file_name;
            $file->move('uploads',  $file_name);
            $data->foto = $file_name;
        }
        $data->nip = $request->nip;
        $data->nama = $request->nama;
        $data->tmp_lahir = $request->tempat;
        $data->tgl_lahir = \Carbon\Carbon::createFromFormat('d/m/Y',$request->tgl)->format('Y-m-d');
        $data->jk = $request->gender;
        $data->agama_id = $request->agama;
        $data->alamat    = $request->alamat;
        $data->jabatan_id = $request->jabatan;
        $data->join =  \Carbon\Carbon::createFromFormat('d/m/Y',$request->join)->format('Y-m-d');
        $data->pendidikan = $request->pendidikan;
        $data->rekening = $request->rekening;
        $data->bank = $request->bank;
        $data->status = $request->status;
        $data->save();

        $ac = new \App\Activity;
        $ac->user_id = \Auth::user()->id;
        $ac->keterangan = 'Pegawai baru '.$request->name ;
        $ac->save();
         return redirect('/workers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['data']= \App\Employee::join('jabatan','jabatan.id','=','pegawai.jabatan_id')->where('pegawai.id',$id)->select('pegawai.*','jabatan.nama as jabatan')->first();
        $data['tgl_lahir'] = \Carbon\Carbon::createFromFormat('Y-m-d',$data['data']['tgl_lahir'])->format("d-m-Y");
        $data['join'] = \Carbon\Carbon::createFromFormat('Y-m-d',$data['data']['join'])->format("d-m-Y");
        $data['agama'] = \App\Region::select('agama')->find($data['data']['agama_id']);
        $data['pendidikan'] = strtoupper($data['data']['pendidikan']);
        $data['cekregion'] = \App\Region::get();
        
        return response($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['id'] = $id;
        $data['user'] = \App\Employee::find($id);
        $data['jabatan'] = \App\Position::orderBy('nama','Asc')->get();
        $data['agama'] = \App\Region::get();
        
        return view('pegawai.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $uuid = Uuid::generate(1);
        $data = \App\Employee::find($id);
        if(Input::File('foto')){
            $file   = Input::file('foto');
            $file_name = $uuid.".".$file->getClientOriginalExtension();
            $path = public_path('uploads/' .$file_name);
            $img = \Image::make($file->getRealPath());
            $img->resize(null, 700, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->save($path);
            // $profil->photo =  $file_name;
            // $file->move('uploads',  $file_name);

        $data->foto = $file_name;
        }
        $data->nip = $request->nip;
        $data->nama = $request->nama;
        $data->tmp_lahir = $request->tempat;
        $data->tgl_lahir = \Carbon\Carbon::createFromFormat('d/m/Y',$request->tgl)->format('Y-m-d');
        $data->jk = $request->gender;
        $data->agama_id = $request->agama;
        $data->alamat    = $request->alamat;
        $data->jabatan_id = $request->jabatan;
        $data->join =  \Carbon\Carbon::createFromFormat('d/m/Y',$request->join)->format('Y-m-d');
        $data->pendidikan = $request->pendidikan;
        $data->rekening = $request->rekening;
        $data->bank = $request->bank;
        $data->status = $request->status;
        $data->save();

        $ac = new \App\Activity;
        $ac->user_id = \Auth::user()->id;
        $ac->keterangan = 'Perbarui data pengguna '.$request->name ;
        $ac->save();

         return redirect('/workers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Employee::find($id)->delete();
        return redirect()->back();
    }
}
