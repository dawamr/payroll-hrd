@extends('layouts.auth')
@section('content')
<div class="container">
        <div class="col-md-12 content-center">
            <div class="card-plain">
                <form action="/setup" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="header">
                        <div class="logo-container">
                            <img class="target" src="/image/logo.png" alt="">
                        </div>
                        <h5>Setup</h5>
                    </div>
                    <div class="content">
                        <div class="input-group input-lg">
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Application Name">
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-account-circle"></i>
                            </span>
                        </div>
                    </div>
                    <div class="footer text-center">
                        <button class="btn btn-primary btn-round btn-lg btn-block " steps="appName" id="btSetup">Next</button> 
                        <button class="btn btn-primary btn-round btn-lg btn-block " steps="appAuth" id="btSetupAuth">Next</button>
                        <button type="submit" class="btn btn-primary btn-round btn-lg btn-block" steps="appSubmit" id="btSetupSubmit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
$(document).ready(function(){
    function showImage(src, target) {
        var fr = new FileReader();
        fr.onload = function(){
            target.src = fr.result;
            $('.target').attr('src',target.src);
        }
        fr.readAsDataURL(src.files[0]);
        console.log(target.src);
    }
    $('#foto').change(function putImage() {
        console.log($(this).val())
        var src = document.getElementById("foto");
        var target = $('.target');
        showImage(src, target);
    });
    $('#btSetup').show();
    $('#btSetupAuth').hide();
    $('#btSetupSubmit').hide();
    $('#btSetup').click((e)=>{
        e.preventDefault();
        if($('#btSetup').attr('steps') == 'appName'){
            $('#btSetup').hide();
            $('#btSetupAuth').show();
            $('#btSetupSubmit').hide();
            $(".logoApp").html($('#nama').val());
            localStorage.setItem("appName", $('#nama').val());
            $('.content').replaceWith(
                '<div class="content">'+
                    '<div class="input-group input-lg">'+
                        '<input type="email" class="form-control" id="email" name="email" placeholder="Application Email" required>'+
                        '<span class="input-group-addon">'+
                            '<i class="zmdi zmdi-account-circle"></i>'+
                       ' </span>'+
                    '</div>'+
                    '<div class="input-group input-lg">'+
                        '<input type="password" class="form-control" id="password" name="password" placeholder="Application Password" required>'+
                        '<span class="input-group-addon">'+
                            '<i class="zmdi zmdi-account-circle"></i>'+
                       ' </span>'+
                    '</div>'+
                '</div>'
            )
        }
    });
    $('#btSetupAuth').click((e)=>{
       e.preventDefault();
        console.log('ok')
        localStorage.setItem("appEmail", $('#email').val());
        localStorage.setItem("appPassword", $('#password').val());
        $('#btSetup').hide();
        $('#btSetupAuth').hide();
        $('#btSetupSubmit').show();
        $('.content').replaceWith(
            '<div class="content">'+
                '<input type="hidden" value="'+localStorage.getItem('appName')+'" name="appName" id="appName">'+
                '<input type="hidden" value="'+localStorage.getItem('appEmail')+'" name="appEmail" id="appEmail">'+
                '<input type="hidden" value="'+localStorage.getItem('appPassword')+'" name="appPassword" id="appPassword"> '+
                '<div class="input-group input-lg">'+
                    '<input type="text" class="form-control" id="appPassword2" name="appPassword2" placeholder="Password Aplication" required>'+
                    '<span class="input-group-addon">'+
                    '<i class="zmdi zmdi-account-circle"></i>'+
                    '</span>'+
                '</div>'+
                '<div class="input-group input-lg">'+
                    '<input type="file" class="form-control" placeholder="Upload logo" name="foto"  id="foto" required>'+
                '</div>'+
            '</div>'
        )
        
    });
    
    
})
</script>
@endsection
