<aside id="leftsidebar" class="sidebar">
    <ul class="nav nav-tabs">
        <li class="nav-item"></li>
        <!-- <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#user"><i class="zmdi zmdi-account m-r-5"></i>Akun</a></li> -->
    </ul>
    <div class="tab-content mt-3">
        <div class="tab-pane stretchRight active" id="dashboard">
            <div class="menu">
                <ul class="list">
                    <li>
                        <div class="user-info">
                            <div class="image"><a href="profile.html"><img src="{{asset('icon/nawakara.jpg')}}" alt="User"></a></div>
                            <div class="detail">
                                <h4>{{\Auth::user()->name}}</h4>
                                <small>{{\Auth::user()->role}}</small>                        
                            </div>
                            {{-- <a title="facebook" href="javascript:void(0);"><i class="zmdi zmdi-facebook"></i></a> --}}
                            {{-- <a title="twitter" href="javascript:void(0);"><i class="zmdi zmdi-twitter"></i></a> --}}
                            {{-- <a title="instagram" href="javascript:void(0);"><i class="zmdi zmdi-instagram"></i></a>                             --}}
                        </div>
                    </li>
                    <li class="header">Navigasi</li>
                    <li> <a href="/dashboard"><i class="zmdi zmdi-home"></i><span>Beranda</span></a>
                    </li>
                    @if(Auth::user()->role == 'admin')
                    <li> <a href="javascript:void(0);" class="menu-toggle"><i class="material-icons">work</i><span>Jabatan</span> </a>
                        <ul class="ml-menu">
                            <li><a href="/position">Semua Jabatan</a></li>
                            <li><a href="/position/create">Jabatan Baru</a></li>
                        </ul>
                    </li>
                    @endif
                    @if(Auth::user()->role == 'admin')
                    <li> <a href="javascript:void(0);" class="menu-toggle"><i class="material-icons">group</i><span>Pegawai</span></a>
                        <ul class="ml-menu">
                            <li><a href="/workers">Semua Pegawai</a> </li>
                            <li><a href="/workers/create">Pegawai Baru</a></li>
                        </ul>
                    </li>
                    @endif
                    @if(Auth::user()->role == 'admin')
                    <li> <a href="javascript:void(0);" class="menu-toggle"><i class="material-icons">date_range</i><span>Absensi</span> </a>
                        <ul class="ml-menu">
                            <li><a href="/presence">Absensi</a></li>
                            <li><a href="/presence/create">Absensi Baru</a></li>
                        </ul>
                    </li>
                    @endif
                    
                    <li> <a href="/salary" ><i class="material-icons">account_balance_wallet</i><span>Penggajian</span> </a>
                    </li>
                                        
                    <!-- <li class="header">Akun</li>
                    <li> <a href="javascript:void(0);" class="menu-toggle"><i class="material-icons">account_circle</i><span>Profile</span></a>
                        <ul class="ml-menu">
                            <li><a href="index-2.html">Akun Pengguna</a> </li>
                            <li><a href="dashboard-rtl.html">Ganti Password</a></li>
                        </ul>
                    </li> -->
                </ul>
            </div>
        </div>
        
    </div>    
</aside>