<?php

use Illuminate\Database\Seeder;

class AgamaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reg = new \App\Region;
        $reg->agama = 'islam';
        $reg->save();

        $reg = new \App\Region;
        $reg->agama = 'hindu';
        $reg->save();

        $reg = new \App\Region;
        $reg->agama = 'buddha';
        $reg->save();

        $reg = new \App\Region;
        $reg->agama = 'kristen';
        $reg->save();

        $reg = new \App\Region;
        $reg->agama = 'katolik';
        $reg->save();

    }
}
